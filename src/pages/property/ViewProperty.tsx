import { useEffect, useState } from 'react';
import { message, Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import {
  propertyDto,
  facilityPropertyDto,
  locationDto,
  spesificationPropertyDto,
} from '../../utils/interface';
import DetailProperty from '../../components/property/DetailProperty';

export default function ViewDetailProperty() {
  const [loading, setLoading] = useState(false);
  const [location, setLocation] = useState<locationDto[]>();
  const [property, setProperty] = useState<propertyDto>();
  const [spesificationProperty, setSpesificationProperty] =
    useState<spesificationPropertyDto[]>();
  const [facilityProperty, setFacilityProperty] =
    useState<facilityPropertyDto[]>();

  const { kode_property } = useParams();
  const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;

  const getPropertyByID = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}property/${kode_property}`
      );

      if (response && response.data.statusCode === 200) {
        return response.data.data;
      }

      if (response && response.data.statusCode === 404) {
        message.error(response.data.message);
      }
    } catch (error) {
      message.error(`failed: ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const getLocations = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}locations`
      );

      if (response && response.data.statusCode === 200) {
        return response.data.data;
      }
    } catch (error) {
      message.error(`failed: ${error}`);
    }
  };

  useEffect(() => {
    Promise.all([getPropertyByID(), getLocations()]).then((values: any) => {
      const { property, facility, spesification } = values[0];
      setProperty({
        kode_property: property.kode_property,
        nama_property: property.nama_property,
        type_property: property.type_property,
        latitude: Number(property.latitude),
        longitude: Number(property.longitude),
        harga_property: property.harga_property,
        harga_sewa_property: property.harga_sewa_property,
        stok: property.stok,
        alamat: property.alamat,
        picture: values[0].picture[0].url_picture,
        createdAt: property.createdAt,
        updatedAt: property.updatedAt,
      });
      let tempFacilityProperty: facilityPropertyDto[] = [];
      let tempSpesificationProperty: spesificationPropertyDto[] = [];
      facility.forEach((val: any) => {
        tempFacilityProperty.push({
          kode_property_facility: val.kode_property_facility,
          kode_facility: val.kode_facility,
          nama_fasilitas: val.nama_fasilitas,
          kode_property: val.kode_property,
          value: val.value,
        });
      });

      spesification.forEach((val: any) => {
        tempSpesificationProperty.push({
          kode_property_spesification: val.kode_property_spesification,
          kode_spesification: val.kode_spesification,
          nama_spesifikasi: val.nama_spesifikasi,
          kode_property: val.kode_property,
          value: val.value,
        });
      });

      setFacilityProperty(tempFacilityProperty);
      setSpesificationProperty(tempSpesificationProperty);
      setFacilityProperty(tempFacilityProperty);
      setLocation(
        values[1].map((val: any) => ({
          kode_lokasi: val.kode_lokasi,
          nama_lokasi: val.nama_lokasi,
          alamat: val.alamat,
          latitude: Number(val.latitude),
          longitude: Number(val.longitude),
          icon: val.icon,
        }))
      );
    });
  }, []);

  return (
    <>
      {!loading ? (
        <>
          <DetailProperty
            property={property}
            facility={facilityProperty}
            location={location}
            spesification={spesificationProperty}
          />
        </>
      ) : (
        <div className="w-screen h-screen absolute flex justify-center items-center bg-[#00000010]">
          <Spin indicator={antIcon} />
        </div>
      )}
    </>
  );
}
