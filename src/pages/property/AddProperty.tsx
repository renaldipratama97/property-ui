import { useState, useEffect } from 'react';
import {
  Form,
  Input,
  InputNumber,
  Skeleton,
  Upload,
  Button,
  Select,
  message,
} from 'antd';
import { useNavigate } from 'react-router-dom';
import { GoogleMap, useJsApiLoader, MarkerF } from '@react-google-maps/api';
import { UploadOutlined } from '@ant-design/icons';
import type { UploadFile } from 'antd/es/upload/interface';
import type { UploadProps } from 'antd/es/upload';
import axios from 'axios';
import { propertyType } from '../../utils/data';

const center = {
  address:
    'Jl. Tilam, Rimba Panjang, Kec. Tambang, Kabupaten Kampar, Riau 28293',
  lat: 0.45854,
  lng: 101.3414875,
};

const AddProperty = () => {
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [spesification, setSpesification] = useState<any>([]);
  const [facilities, setFacilities] = useState<any>([]);

  const [form] = Form.useForm();
  const navigate = useNavigate();

  const onFinish = async (values: any) => {
    try {
      let response = null;
      if (location.search) {
        const payload = {
          kode_property: location.search.split('=')[1],
          nama_property: values.nama_property,
          type_property: values.type_property,
          stock_property: values.stock_property,
          latitude: values.latitude,
          longitude: values.longitude,
          alamat: values.alamat_property,
          harga_property: values.harga_property,
          harga_sewa_property: values.harga_sewa_property,
        };

        response = await axios.put(
          `${import.meta.env.VITE_BASE_URL_API}property`,
          payload
        );
      } else {
        const formData = new FormData();
        formData.append('nama_property', values.nama_property);
        formData.append('stock_property', values.stock_property);
        formData.append('type_property', values.type_property);
        formData.append('latitude', values.latitude);
        formData.append('longitude', values.longitude);
        formData.append('alamat_property', values.alamat_property);
        formData.append('harga_property', values.harga_property);
        formData.append('harga_sewa_property', values.harga_sewa_property);
        formData.append('spesifikasi', JSON.stringify(values.spesifikasi));
        formData.append('fasilitas', JSON.stringify(values.fasilitas));
        formData.append('file', values.image_property.file);

        response = await axios.post(
          `${import.meta.env.VITE_BASE_URL_API}property`,
          formData
        );
      }

      if (response && response.data.statusCode === 422) {
        message.error(response.data.message);
      }

      if (
        response &&
        (response.data.statusCode === 201 || response.data.statusCode === 200)
      ) {
        message.success(response.data.message);
        navigate('/app/property');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getLatLng = (marker: any) => {
    const latlng = marker.latLng;

    form.setFieldsValue({
      latitude: latlng.lat(),
      longitude: latlng.lng(),
    });
  };

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList, file]);
      return false;
    },
    fileList,
  };

  const getPropertyByKode = async (kode: string) => {
    const response = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}property/${kode}`
    );

    const value = response.data.data.property;

    form.setFieldsValue({
      nama_property: value.nama_property,
      type_property: value.type_property,
      stock_property: value.stok,
      latitude: value.latitude,
      longitude: value.longitude,
      alamat_property: value.alamat,
      harga_sewa_property: value.harga_sewa_property,
      harga_property: value.harga_property,
    });
  };

  const getFacilities = async () => {
    const fetchFacilities = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}facilities`
    );

    if (fetchFacilities) {
      setFacilities(fetchFacilities.data.data);
    }
  };

  const getSpesification = async () => {
    const fetchSpesification = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}spesifications`
    );

    if (fetchSpesification) {
      setSpesification(fetchSpesification.data.data);
    }
  };

  useEffect(() => {
    getFacilities();
    getSpesification();
  }, []);

  useEffect(() => {
    if (location.search) {
      getPropertyByKode(location.search.split('=')[1]);
    }
  }, [location]);

  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: import.meta.env.VITE_GMAPS_API_KEY,
    libraries: ['places'],
  });

  if (!isLoaded) {
    return <Skeleton />;
  }

  return (
    <>
      <div className="flex flex-col justify-between">
        <h3 className="text-[18px] font-extrabold">Form Data Property</h3>
      </div>

      <div className="w-[100%] mt-5">
        <Form
          form={form}
          name="property_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          {/* Section 1 */}
          <div className="bg-[#00000025] w-full rounded-lg p-3">
            <div className="w-6/12">
              <div className="flex flex-col justify-between mb-5">
                <h3 className="text-[18px] font-bold">Detail Property</h3>
                <span className="text-[14px] font-light text-[#025595]">
                  Please fill out the form below
                </span>
              </div>
              <Form.Item
                label="Nama Property"
                name="nama_property"
                rules={[
                  {
                    required: true,
                    message: 'Please input property name!',
                  },
                ]}
              >
                <Input size="large" />
              </Form.Item>
              <Form.Item
                label="Type Property"
                name="type_property"
                rules={[
                  {
                    required: true,
                    message: 'Please input property type!',
                  },
                ]}
              >
                <Select
                  placeholder="Select Type"
                  style={{ width: '100%' }}
                  options={propertyType}
                  size="large"
                />
              </Form.Item>
              <Form.Item
                label="Stok"
                name="stock_property"
                rules={[
                  {
                    required: true,
                    message: 'Please input property stock!',
                  },
                ]}
              >
                <InputNumber className="w-full" size="large" min={1} />
              </Form.Item>
              {!location.search && (
                <Form.Item
                  label="Image Property"
                  name="image_property"
                  rules={[
                    {
                      required: true,
                      message: 'Please input property images!',
                    },
                  ]}
                >
                  <Upload {...props} accept=".jpg, .jpeg, .png">
                    <Button icon={<UploadOutlined />}>Select File</Button>
                  </Upload>
                </Form.Item>
              )}
            </div>
          </div>

          {/* Section 2 */}
          <div className="bg-[#00000025] w-full rounded-lg p-3 mt-3">
            <div className="flex flex-col justify-between mb-5 w-12/12">
              <h3 className="text-[18px] font-bold">Location Property</h3>
              <span className="text-[14px] font-light text-[#025595]">
                Please fill out the form below
              </span>
            </div>
            <div className="w-12/12 flex gap-3">
              <div className="w-6/12">
                <Form.Item
                  label="Alamat Property"
                  name="alamat_property"
                  initialValue={center.address}
                  rules={[
                    {
                      required: true,
                      message: 'Please input property address!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="Latitude"
                  name="latitude"
                  initialValue={center.lat}
                  rules={[
                    {
                      required: true,
                      message: 'Please input latitude!',
                    },
                  ]}
                >
                  <Input size="large" disabled />
                </Form.Item>
                <Form.Item
                  label="Longitude"
                  name="longitude"
                  initialValue={center.lng}
                  rules={[
                    {
                      required: true,
                      message: 'Please input longitude!',
                    },
                  ]}
                >
                  <Input size="large" disabled />
                </Form.Item>
              </div>
              <div className="w-6/12 h-96 rounded-lg bg-white relative">
                <div className="absolute w-[100%] h-[100%]">
                  <GoogleMap
                    center={center}
                    mapContainerClassName="wrapper-maps"
                    zoom={15}
                    mapContainerStyle={{
                      width: '100%',
                      height: '100%',
                    }}
                    options={{
                      zoomControl: true,
                      streetViewControl: false,
                      mapTypeControl: false,
                      fullscreenControl: false,
                    }}
                  >
                    <MarkerF
                      position={center}
                      draggable={true}
                      onDrag={(marker) => getLatLng(marker)}
                    />
                  </GoogleMap>
                </div>
              </div>
            </div>
          </div>

          {/* Section 3 */}
          {!location.search && (
            <div className="bg-[#00000025] w-full rounded-lg p-3 mt-3">
              <div className="w-6/12">
                <div className="flex flex-col justify-between mb-5">
                  <h3 className="text-[18px] font-bold">Facility Property</h3>
                  <span className="text-[14px] font-light text-[#025595]">
                    Please fill out the form below
                  </span>
                </div>
                {facilities.length &&
                  facilities.map((val: any, idx: number) => (
                    <Form.Item
                      key={idx}
                      label={val.nama_fasilitas}
                      name={['fasilitas', idx]}
                      rules={[
                        {
                          required: true,
                          message: `Please input ${val.nama_fasilitas}!`,
                        },
                      ]}
                    >
                      <Input size="large" />
                    </Form.Item>
                  ))}
              </div>
            </div>
          )}

          {/* Section 4*/}
          {!location.search && (
            <div className="bg-[#00000025] w-full rounded-lg p-3 mt-3">
              <div className="w-6/12">
                <div className="flex flex-col justify-between mb-5">
                  <h3 className="text-[18px] font-bold">
                    Spesification Property
                  </h3>
                  <span className="text-[14px] font-light text-[#025595]">
                    Please fill out the form below
                  </span>
                </div>
                {spesification.length &&
                  spesification.map((val: any, idx: number) => (
                    <Form.Item
                      key={idx}
                      label={val.nama_spesifikasi}
                      name={['spesifikasi', idx]}
                      rules={[
                        {
                          required: true,
                          message: `Please input ${val.nama_spesifikasi}!`,
                        },
                      ]}
                    >
                      <Input size="large" />
                    </Form.Item>
                  ))}
              </div>
            </div>
          )}

          {/* Section 5 */}
          <div className="bg-[#00000025] w-full rounded-lg p-3 mt-3">
            <div className="w-6/12">
              <div className="flex flex-col justify-between mb-5">
                <h3 className="text-[18px] font-bold">Price Property</h3>
                <span className="text-[14px] font-light text-[#025595]">
                  Please fill out the form below
                </span>
              </div>
              <Form.Item
                label="Harga Sewa Property (Perbulan)"
                name="harga_sewa_property"
                rules={[
                  {
                    required: true,
                    message: 'Please input property rent price!',
                  },
                ]}
              >
                <InputNumber
                  className="w-full"
                  size="large"
                  prefix="Rp"
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                />
              </Form.Item>
              <Form.Item
                label="Harga Property"
                name="harga_property"
                rules={[
                  {
                    required: true,
                    message: 'Please input property price!',
                  },
                ]}
              >
                <InputNumber
                  className="w-full"
                  size="large"
                  prefix="Rp"
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                />
              </Form.Item>
            </div>
          </div>

          <Form.Item>
            <div className="flex justify-end items-center w-full gap-3 mt-5">
              <button
                type="reset"
                className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
              >
                Clear
              </button>
              <button
                type="submit"
                className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
              >
                {!location.search ? 'Simpan' : 'Update'}
              </button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default AddProperty;
