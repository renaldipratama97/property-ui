import { useState, useEffect } from 'react';
import { Form, Input, Select, message, Skeleton } from 'antd';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

interface EmployeesType {
  value: string;
  label: string;
}

const AddUser = () => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [employees, setEmployees] = useState<EmployeesType[]>([]);

  const onFinish = async (values: any) => {
    try {
      let response = null;
      const payload = {
        karyawan: values.karyawan,
        username: values.username,
        password: values.password,
        role_user: values.role_user,
      };

      response = await axios.post(
        `${import.meta.env.VITE_BASE_URL_API}user`,
        payload
      );

      if (response && response.data.statusCode === 999) {
        message.error(response.data.message);
      }

      if (response && response.data.statusCode === 201) {
        message.success(response.data.message);
        navigate('/app/user');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getEmployees = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}employees`
      );

      if (response && response.data.statusCode === 200) {
        setEmployees(
          response.data.data.map((val: any) => ({
            value: val.kode_karyawan,
            label: val.nama_karyawan,
          }))
        );
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getEmployees();
  }, []);

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Tambah Data User</h3>
      </div>

      <div className="w-[50%] mt-5">
        {loading ? (
          <Skeleton />
        ) : (
          <Form
            form={form}
            name="user_form"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Karyawan"
              name="karyawan"
              rules={[
                {
                  required: true,
                  message: 'Please select karyawan!',
                },
              ]}
            >
              <Select
                placeholder="Select Karyawan"
                style={{ width: '100%' }}
                options={employees}
              />
            </Form.Item>
            <Form.Item
              label="Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input username!',
                },
                {
                  pattern: new RegExp('^\\S*$'),
                  message: 'Username tidak boleh menggunakan spasi!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input password!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="Role User"
              name="role_user"
              rules={[
                {
                  required: true,
                  message: 'Please input role user!',
                },
              ]}
            >
              <Select
                placeholder="Select Role User"
                style={{ width: '100%' }}
                options={[
                  { value: 'Administrator', label: 'Administrator' },
                  { value: 'Direktur', label: 'Direktur' },
                  { value: 'Sales', label: 'Sales' },
                ]}
              />
            </Form.Item>
            <Form.Item>
              <div className="flex justify-end items-center w-full gap-3">
                <button
                  type="reset"
                  className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
                >
                  Clear
                </button>
                <button
                  type="submit"
                  className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
                >
                  Simpan
                </button>
              </div>
            </Form.Item>
          </Form>
        )}
      </div>
    </>
  );
};

export default AddUser;
