import { useState, useEffect } from "react";
import { Table, Button, message } from "antd";
import type { ColumnsType } from "antd/es/table";
import { NavLink } from "react-router-dom";
import { DeleteFilled, EditFilled } from "@ant-design/icons";
import axios from "axios";

interface DataType {
  key: number;
  no: number;
  kode_user: string;
  nama_karyawan: string;
  username: string;
  role_user: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState(false);

  const getUsers = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}users`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_user: val.kode_user,
              nama_karyawan: val.nama_karyawan,
              username: val.username,
              role_user: val.role_user,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const deleteUser = async (value: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `${import.meta.env.VITE_BASE_URL_API}user/${value}`
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getUsers();
    }
  };

  useEffect(() => {
    getUsers();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
      width: 100,
      align: "center",
    },
    {
      title: "Nama Karyawan",
      dataIndex: "nama_karyawan",
      width: "30%",
      align: "center",
    },
    {
      title: "Username",
      dataIndex: "username",
      width: "20%",
      align: "center",
    },
    {
      title: "Role User",
      dataIndex: "role_user",
      width: "20%",
      align: "center",
    },
    {
      title: "Action",
      dataIndex: "kode_user",
      key: "x",
      align: "center",
      render: (value) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* <Button type="primary" ghost className="btn-custom">
              <EditFilled />
            </Button> */}
            <Button
              type="primary"
              danger
              ghost
              onClick={() => deleteUser(value)}
            >
              <DeleteFilled />
            </Button>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Data User</h3>
        <NavLink to="/app/user/tambah-user">
          <button className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]">
            Tambah Data
          </button>
        </NavLink>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10 }}
        scroll={{ y: 400 }}
        loading={loading}
      />
    </>
  );
};

export default index;
