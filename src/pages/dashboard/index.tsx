import { useState, useEffect } from "react";
import { TbTools } from "react-icons/tb";
import { RiFolderSettingsFill, RiBuilding4Fill } from "react-icons/ri";
import { BsFileImageFill } from "react-icons/bs";
import axios from "axios";
import { message } from "antd";
import { Link } from "react-router-dom";

interface CountDto {
  facilities: number;
  spesification: number;
  brosur: number;
  property: number;
}

const index = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<CountDto>();

  const getCount = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}global/count`
      );

      if (response && response.data.data) {
        setData(response.data.data);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCount();
  }, []);

  return (
    <>
      <div className="grid grid-cols-4 grid-rows-[150px] gap-3">
        <Link to="/app/fasilitas">
          <div className="flex items-center gap-5 border border-[#d9d9d9] rounded-md py-2 px-3 cursor-pointer transition-all hover:scale-105 hover:bg-[#025595]">
            <div className="icon">
              <TbTools className="text-[80px] text-gray-400" />
            </div>
            <div className="box">
              <span className="text-[40px] font-bold text-gray-400 font-syne">
                {data?.facilities}
              </span>
              <h3 className="text-[20px] font-bold mb-5 text-gray-500 font-syne">
                Fasilitas
              </h3>
            </div>
          </div>
        </Link>
        <Link to="/app/spesifikasi">
          <div className="flex items-center gap-5 border border-[#d9d9d9] rounded-md py-2 px-3 cursor-pointer transition-all hover:scale-105 hover:bg-[#025595]">
            <div className="icon">
              <RiFolderSettingsFill className="text-[80px] text-gray-400" />
            </div>
            <div className="box">
              <span className="text-[40px] font-bold text-gray-400 font-syne">
                {data?.spesification}
              </span>
              <h3 className="text-[20px] font-bold mb-5 text-gray-500 font-syne">
                Spesifikasi
              </h3>
            </div>
          </div>
        </Link>
        <Link to="/app/brosur">
          <div className="flex items-center gap-5 border border-[#d9d9d9] rounded-md py-2 px-3 cursor-pointer transition-all hover:scale-105 hover:bg-[#025595]">
            <div className="icon">
              <BsFileImageFill className="text-[80px] text-gray-400" />
            </div>
            <div className="box">
              <span className="text-[40px] font-bold text-gray-400 font-syne">
                {data?.brosur}
              </span>
              <h3 className="text-[20px] font-bold mb-5 text-gray-500 font-syne">
                Brosur
              </h3>
            </div>
          </div>
        </Link>
        <Link to="/app/property">
          <div className="flex items-center gap-5 border border-[#d9d9d9] rounded-md py-2 px-3 cursor-pointer transition-all hover:scale-105 hover:bg-[#025595]">
            <div className="icon">
              <RiBuilding4Fill className="text-[80px] text-gray-400" />
            </div>
            <div className="box">
              <span className="text-[40px] font-bold text-gray-400 font-syne">
                {data?.property}
              </span>
              <h3 className="text-[20px] font-bold mb-5 text-gray-500 font-syne">
                Property
              </h3>
            </div>
          </div>
        </Link>
      </div>
    </>
  );
};

export default index;
