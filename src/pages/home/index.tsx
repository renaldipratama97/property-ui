import { Modal, message } from 'antd';
import BaseHeader from '../../components/base/BaseHeader';
import PropertyCard from '../../components/card/PropertyCard';
// import DreamHomeComponent from '../../components/property/DreamHomeComponent';
import CorouselProperty from '../../components/property/CorouselProperty';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { ImDownload } from 'react-icons/im';
import { TbError404Off } from 'react-icons/tb';
import { BsWhatsapp } from 'react-icons/bs';
import dayjs from 'dayjs';
import { saveAs } from 'file-saver';
import BaseFooter from '../../components/base/BaseFooter';

const index = () => {
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [brosurList, setBrosurList] = useState<any>([]);

  const getBrosurs = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}brosur/active`
      );

      if (response && response.data.statusCode === 200) {
        if (response.data.data.length) {
          setBrosurList(response.data.data);
        }
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const downloadBrosur = (value: string) => {
    saveAs(`${import.meta.env.VITE_BASE_URL_API}${value}`, 'brosur.pdf');
  };

  useEffect(() => {
    getBrosurs();
  }, []);

  return (
    <>
      <BaseHeader setOpenModal={setOpenModal} value={openModal} />
      {/* <DreamHomeComponent /> */}
      <CorouselProperty />
      <PropertyCard />
      <BaseFooter />

      <a
        href="https://api.whatsapp.com/send?phone=+6282272188684&text=Saya%20mau%20beli%20rumah"
        target="_blank"
        className="fixed group bottom-10 right-10 h-14 w-14 rounded-xl cursor-pointer bg-green-500 hover:bg-green-600 p-3"
      >
        <BsWhatsapp className="w-full h-full text-white" />
      </a>

      <Modal
        open={openModal}
        footer={false}
        closable={false}
        onCancel={() => setOpenModal(!openModal)}
      >
        <div className="w-full text-center">
          <h1 className="text-2xl font-bold">LIST BROSUR</h1>
        </div>
        <div className="flex flex-col gap-3 mt-8">
          {brosurList.length ? (
            brosurList.map((val: any, idx: any) => (
              <div
                key={idx}
                className="flex justify-between items-center w-full border border-gray-200 rounded-md py-4 px-2"
              >
                <div className="flex justify-center items-center gap-3">
                  <iframe
                    src={`${import.meta.env.VITE_BASE_URL_API}${
                      val.url_brosur
                    }`}
                    frameBorder="0"
                    className="w-10 h-10"
                  ></iframe>
                  <div className="flex flex-col font-syne">
                    <span className="text-[15px]">{val.nama_brosur}</span>
                    <span className="text-[10px] text-gray-500">
                      created : {dayjs(val.createdAt).format('DD MMMM, YYYY')}
                    </span>
                  </div>
                </div>
                <div>
                  <button
                    onClick={() => downloadBrosur(val.url_brosur)}
                    className="text-[20px] px-5 py-3 bg-transparent text-[#146C94] hover:bg-[#146C94] hover:text-white rounded-md"
                  >
                    <ImDownload />
                  </button>
                </div>
              </div>
            ))
          ) : (
            <div className="flex flex-col justify-center items-center">
              <TbError404Off className="text-[150px] text-gray-400" />
              <p className="text-xl font-extrabold text-gray-400 font-syne">
                For now, brochures do not exist
              </p>
            </div>
          )}
        </div>
      </Modal>
    </>
  );
};

export default index;
