import { useState, useEffect } from "react";
import { Table, Button, message } from "antd";
import type { ColumnsType } from "antd/es/table";
import { Link, NavLink } from "react-router-dom";
import { DeleteFilled, EditFilled } from "@ant-design/icons";
import axios from "axios";

interface DataType {
  key: number;
  no: number;
  kode_fasilitas: string;
  nama_fasilitas: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState(false);

  const getFacilities = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}facilities`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_fasilitas: val.kode_fasilitas,
              nama_fasilitas: val.nama_fasilitas,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const deleteFacility = async (value: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `${import.meta.env.VITE_BASE_URL_API}facility/${value}`
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getFacilities();
    }
  };

  useEffect(() => {
    getFacilities();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: "No",
      dataIndex: "no",
      width: 100,
      align: "center",
    },
    {
      title: "Nama Fasilitas",
      dataIndex: "nama_fasilitas",
      width: "70%",
      align: "center",
    },
    {
      title: "Action",
      dataIndex: "kode_fasilitas",
      key: "x",
      align: "center",
      render: (value) => {
        return (
          <div className="flex gap-2 justify-center">
            <Link to={`/app/fasilitas/form-fasilitas?kode=${value}`}>
              <Button type="primary" ghost className="btn-custom">
                <EditFilled />
              </Button>
            </Link>
            <Button
              type="primary"
              danger
              ghost
              onClick={() => deleteFacility(value)}
            >
              <DeleteFilled />
            </Button>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Data Fasilitas</h3>
        <NavLink to="/app/fasilitas/form-fasilitas">
          <button className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]">
            Tambah Data
          </button>
        </NavLink>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10 }}
        scroll={{ y: 400 }}
        loading={loading}
      />
    </>
  );
};

export default index;
