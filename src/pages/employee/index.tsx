import { useState, useEffect } from 'react';
import { Table, Button, message } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Link, NavLink } from 'react-router-dom';
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import axios from 'axios';

interface DataType {
  key: number;
  no: number;
  kode_karyawan: string;
  nik: string;
  nama_karyawan: string;
  jabatan: string;
  no_handphone: string;
  alamat: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState(false);

  const getEmployees = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}employees`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_karyawan: val.kode_karyawan,
              nik: val.nik,
              nama_karyawan: val.nama_karyawan,
              jabatan: val.jabatan,
              no_handphone: val.no_handphone,
              alamat: val.alamat_lengkap,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const deleteEmployee = async (value: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `${import.meta.env.VITE_BASE_URL_API}employee/${value}`
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getEmployees();
    }
  };

  useEffect(() => {
    getEmployees();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: 'No',
      dataIndex: 'no',
      width: 100,
      align: 'center',
    },
    {
      title: 'N I K',
      dataIndex: 'nik',
      align: 'center',
    },
    {
      title: 'Nama Karyawan',
      dataIndex: 'nama_karyawan',
      align: 'center',
    },
    {
      title: 'Jabatan',
      dataIndex: 'jabatan',
      align: 'center',
    },
    {
      title: 'No Handphone',
      dataIndex: 'no_handphone',
      align: 'center',
    },
    {
      title: 'Alamat',
      dataIndex: 'alamat',
      align: 'center',
    },
    {
      title: 'Action',
      dataIndex: 'kode_karyawan',
      key: 'x',
      align: 'center',
      render: (value) => {
        return (
          <div className="flex gap-2 justify-center">
            <Link to={`/app/karyawan/form-karyawan?kode=${value}`}>
              <Button type="primary" ghost className="btn-custom">
                <EditFilled />
              </Button>
            </Link>
            <Button
              type="primary"
              danger
              ghost
              onClick={() => deleteEmployee(value)}
            >
              <DeleteFilled />
            </Button>
          </div>
        );
      },
    },
  ];
  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Data Karyawan</h3>
        <NavLink to="/app/karyawan/form-karyawan">
          <button className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]">
            Tambah Data
          </button>
        </NavLink>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10 }}
        scroll={{ y: 400 }}
        loading={loading}
      />
    </>
  );
};

export default index;
