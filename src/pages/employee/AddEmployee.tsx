import { useState, useEffect } from 'react';
import { Form, Input, Button, Upload, DatePicker, Select, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import type { UploadFile, UploadProps } from 'antd/es/upload/interface';
import dayjs from 'dayjs';
import axios from 'axios';

const AddEmployee = () => {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [fileList, setFileList] = useState<UploadFile[]>([]);

  const onFinish = async (values: any) => {
    try {
      let response = null;
      let payload = {
        kode_karyawan: location.search ? location.search.split('=')[1] : null,
        nik: values.nik,
        nama_karyawan: values.employee_name,
        tempat_lahir: values.place_of_birth,
        tgl_lahir: location.search ? null : values.date_of_birth,
        jenis_kelamin: values.gender,
        jabatan: values.position,
        no_handphone: values.phonenumber,
        alamat_lengkap: values.address,
        file: values.picture ? values.picture.file : null,
      };
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      };

      if (location.search) {
        response = await axios.put(
          `${import.meta.env.VITE_BASE_URL_API}employee`,
          payload,
          config
        );
      } else {
        response = await axios.post(
          `${import.meta.env.VITE_BASE_URL_API}employee`,
          payload,
          config
        );
      }

      if (response && response.data.statusCode === 422) {
        message.error(response.data.message);
      }

      if (
        response &&
        (response.data.statusCode === 201 || response.data.statusCode === 200)
      ) {
        message.success(response.data.message);
        navigate('/app/karyawan');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getEmployeeById = async (value: string) => {
    const fetch = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}employee/${value}`
    );

    if (fetch) {
      const {
        nik,
        nama_karyawan,
        tempat_lahir,
        jenis_kelamin,
        jabatan,
        no_handphone,
        alamat_lengkap,
      } = fetch.data.data;

      form.setFieldsValue({
        nik: nik,
        employee_name: nama_karyawan,
        place_of_birth: tempat_lahir,
        gender: jenis_kelamin,
        position: jabatan,
        phonenumber: no_handphone,
        address: alamat_lengkap,
      });
    }
  };

  useEffect(() => {
    if (location.search) {
      getEmployeeById(location.search.split('=')[1]);
    }
  }, [location]);

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Tambah Data Karyawan</h3>
      </div>

      <div className="w-[50%] mt-5">
        <Form
          form={form}
          name="employee_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="N I K"
            name="nik"
            rules={[
              {
                required: true,
                message: 'Please input employee id!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Nama Lengkap"
            name="employee_name"
            rules={[
              {
                required: true,
                message: 'Please input employee name!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Tempat Lahir"
            name="place_of_birth"
            rules={[
              {
                required: true,
                message: 'Please input place of birth!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          {!location.search && (
            <Form.Item
              label="Tanggal Lahir"
              name="date_of_birth"
              rules={[
                {
                  required: true,
                  message: 'Please input date of birth!',
                },
              ]}
            >
              <DatePicker format="YYYY-MM-DD" className="w-[100%]" />
            </Form.Item>
          )}
          <Form.Item
            label="Jenis Kelamin"
            name="gender"
            rules={[
              {
                required: true,
                message: 'Please input employee gender!',
              },
            ]}
          >
            <Select
              placeholder="Select Gender"
              style={{ width: '100%' }}
              options={[
                { value: 'Pria', label: 'Pria' },
                { value: 'Wanita', label: 'Wanita' },
              ]}
            />
          </Form.Item>
          <Form.Item
            label="Jabatan"
            name="position"
            rules={[
              {
                required: true,
                message: 'Please input employee position!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="No Handphone"
            name="phonenumber"
            rules={[
              {
                required: true,
                message: 'Please input employee phone number!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Alamat Lengkap"
            name="address"
            rules={[
              {
                required: true,
                message: 'Please input employee address!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Picture"
            name="picture"
            rules={[
              {
                required: location.search ? false : true,
                message: 'Please input employee picture!',
              },
            ]}
          >
            <Upload {...props} accept=".jpg, .jpeg, .png">
              <Button icon={<UploadOutlined />}>Select File</Button>
            </Upload>
          </Form.Item>
          <Form.Item>
            <div className="flex justify-end items-center w-full gap-3">
              <button
                type="reset"
                className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
              >
                Clear
              </button>
              <button
                type="submit"
                className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
              >
                {location.search ? 'Update' : 'Simpan'}
              </button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default AddEmployee;
