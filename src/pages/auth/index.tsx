import { Form, Input, message } from 'antd';
import axios from 'axios';
import { useSignIn } from 'react-auth-kit';
import { useNavigate } from 'react-router-dom';

const index = () => {
  const [form] = Form.useForm();
  const signIn = useSignIn();
  const navigate = useNavigate();

  const onFinish = async (values: any) => {
    try {
      const payload = {
        username: values.username,
        password: values.password,
      };
      const response = await axios.post(
        `${import.meta.env.VITE_BASE_URL_API}auth/signin`,
        payload
      );

      if (response && response.status === 200) {
        if (response.data.statusCode === 404) {
          message.error(response.data.message);
        } else if (response.data.statusCode === 400) {
          message.error(response.data.message);
        } else if (response.data.statusCode === 200) {
          localStorage.setItem('picture', response.data.data.picture);
          localStorage.setItem('user_level', response.data.data.role_user);
          localStorage.setItem('employee', response.data.data.kode_karyawan);
          signIn({
            token: response.data.data.token,
            expiresIn: 86400,
            tokenType: 'Bearer',
            authState: {
              kode_user: response.data.data.kode_user,
              kode_karyawan: response.data.data.kode_karyawan,
              nama_karyawan: response.data.data.nama_karyawan,
              username: response.data.data.username,
              role_user: response.data.data.role_user,
              picture: response.data.data.picture,
            },
          });
          message.success(response.data.message);
          navigate('/app/beranda');
        }
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  return (
    <>
      <main className="w-[100vw] h-[100vh] bg-white flex justify-center items-center">
        <div className="w-4/12 py-20 bg-[#dedede] rounded-lg px-10">
          <div className="flex flex-col justify-center items-center">
            <img src="/logo.svg" alt="logo-icon" width={200} height={100} />
            <h1 className="font-extrabold text-[#025595] text-3xl mt-2">
              PT. Aricha Propertindo
            </h1>
          </div>
          <Form
            form={form}
            name="user_form"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            className="mt-8"
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input username!',
                },
                {
                  pattern: new RegExp('^\\S*$'),
                  message: 'Username tidak boleh menggunakan spasi!',
                },
              ]}
            >
              <Input size="large" placeholder="Enter your username" />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input password!',
                },
              ]}
            >
              <Input.Password size="large" placeholder="Enter your password" />
            </Form.Item>
            <Form.Item>
              <div className="flex justify-end items-center w-full gap-3">
                <button
                  type="submit"
                  className="bg-[#025595] border-none font-bold hover:bg-[#023f95] px-10 py-4 text-gray-200 rounded-xl text-[12px]"
                >
                  Sign in
                </button>
              </div>
            </Form.Item>
          </Form>
        </div>
      </main>
    </>
  );
};

export default index;
