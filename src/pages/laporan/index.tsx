import { useState, useEffect } from 'react';
import { Table, message } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import axios from 'axios';
import { BsFillFilePdfFill } from 'react-icons/bs';
import { saveAs } from 'file-saver';
import dayjs from 'dayjs';

interface DataType {
  key: number;
  no: number;
  kode_transaksi_sewa: string;
  kode_property: string;
  nama_property: string;
  jenis_pembayaran: string;
  jatuh_tempo: string;
  total_bayar: number;
  nik: string;
  nama_lengkap: string;
  email: string;
  no_handphone: string;
  alamat_lengkap: string;
  file_pendukung: string;
  nama_sales: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState(false);

  const getLocations = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}transaksi-sewa`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_transaksi_sewa: val.kode_transaksi_sewa,
              kode_property: val.kode_property,
              nama_property: val.nama_property,
              jenis_pembayaran: val.jenis_pembayaran,
              jatuh_tempo: val.jatuh_tempo,
              total_bayar: val.total_bayar,
              nik: val.nik,
              nama_lengkap: val.nama_lengkap,
              email: val.email,
              no_handphone: val.no_handphone,
              alamat_lengkap: val.alamat_lengkap,
              file_pendukung: val.file_pendukung,
              nama_sales: val.nama_karyawan,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const downloadPDF = () => {
    axios
      .post(`${import.meta.env.VITE_BASE_URL_API}global/create-pdf-transaction`)
      .then(() =>
        axios.get(`${import.meta.env.VITE_BASE_URL_API}global/fetch-pdf`, {
          responseType: 'blob',
        })
      )
      .then((res) => {
        const pdfBlob = new Blob([res.data], { type: 'application/pdf' });

        saveAs(pdfBlob, 'transaction.pdf');
      });
  };

  useEffect(() => {
    getLocations();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: 'No',
      dataIndex: 'no',
      width: '5%',
      align: 'center',
    },
    {
      title: 'Nama Property',
      dataIndex: 'nama_property',
      width: '10%',
      align: 'left',
    },
    {
      title: 'Nama Sales',
      dataIndex: 'nama_sales',
      width: '10%',
      align: 'left',
    },
    {
      title: 'Jenis Pembayaran',
      dataIndex: 'jenis_pembayaran',
      width: '10%',
      align: 'left',
    },
    {
      title: 'Jatuh Tempo',
      dataIndex: 'jatuh_tempo',
      width: '10%',
      align: 'left',
      render: (value) => {
        return dayjs(value).format('D MMMM YYYY');
      },
    },
    {
      title: 'Total Bayar',
      dataIndex: 'total_bayar',
      width: '10%',
      align: 'left',
      render: (value) => {
        return <span>Rp. {value.toLocaleString('en-US')}</span>;
      },
    },
    {
      title: 'Nama Lengkap',
      dataIndex: 'nama_lengkap',
      width: '10%',
      align: 'center',
    },
    {
      title: 'No Handphone',
      dataIndex: 'no_handphone',
      width: '10%',
      align: 'left',
    },
    {
      title: 'Alamat Lengkap',
      dataIndex: 'no_handphone',
      width: '20%',
      align: 'left',
    },
  ];

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">
          Laporan Transaksi Sewa Property
        </h3>
        <button
          onClick={() => downloadPDF()}
          className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px] flex justify-center items-center gap-2"
        >
          <BsFillFilePdfFill className="text-lg" /> Export PDF
        </button>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={false}
        scroll={{ y: 400 }}
        loading={loading}
      />
    </>
  );
};

export default index;
