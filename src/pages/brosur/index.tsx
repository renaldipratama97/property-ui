import { useState, useEffect } from 'react';
import { Table, Button, Checkbox, message, Modal } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Link, NavLink } from 'react-router-dom';
import { DeleteFilled, EditFilled, FileImageOutlined } from '@ant-design/icons';
import axios from 'axios';

interface DataType {
  key: number;
  no: number;
  kode_brosur: string;
  nama_brosur: string;
  status_brosur: number;
  url_brosur: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [documentActive, setDocumentActive] = useState<string>('');

  const getBrosur = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}brosurs`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_brosur: val.kode_brosur,
              nama_brosur: val.nama_brosur,
              status_brosur: val.status_brosur,
              url_brosur: val.url_brosur,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const deleteBrosur = async (value: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `${import.meta.env.VITE_BASE_URL_API}brosur/${value}`
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getBrosur();
    }
  };

  const changeStatusBrosur = async (kode_brosur: string, status: number) => {
    setLoading(true);
    try {
      const response = await axios.put(
        `${import.meta.env.VITE_BASE_URL_API}brosur/change-status`,
        { kode_brosur, status: status === 1 ? 0 : 1 }
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getBrosur();
    }
  };

  const onOpenModal = (url: string) => {
    setOpenModal(true);
    setDocumentActive(url);
  };

  const onCloseModal = () => {
    setOpenModal(false);
    setDocumentActive('');
  };

  useEffect(() => {
    getBrosur();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: 'No',
      dataIndex: 'no',
      width: 100,
      align: 'center',
    },
    {
      title: 'Nama Brosur',
      dataIndex: 'nama_brosur',
      width: '30%',
      align: 'center',
    },
    {
      title: 'File Brosur',
      dataIndex: 'url_brosur',
      width: '30%',
      align: 'center',
      render: (url) => {
        return (
          <Button
            type="dashed"
            block
            className="flex justify-center items-center"
            onClick={() => onOpenModal(url)}
          >
            <FileImageOutlined /> Lihat Brosur
          </Button>
        );
      },
    },
    {
      title: 'Status Brosur',
      dataIndex: 'status_brosur',
      width: '15%',
      align: 'left',
      render: (data, row) => {
        return (
          <Checkbox
            checked={data === 1 ? true : false}
            onChange={() => changeStatusBrosur(row.kode_brosur, data)}
          >
            {data === 1 ? 'Active' : 'Inactive'}
          </Checkbox>
        );
      },
    },
    {
      title: 'Action',
      dataIndex: 'kode_brosur',
      key: 'x',
      align: 'center',
      render: (kode) => {
        return (
          <div className="flex gap-2 justify-center">
            <Link to={`/app/brosur/form-brosur?kode=${kode}`}>
              <Button type="primary" ghost className="btn-custom">
                <EditFilled />
              </Button>
            </Link>
            <Button
              type="primary"
              danger
              ghost
              onClick={() => deleteBrosur(kode)}
            >
              <DeleteFilled />
            </Button>
          </div>
        );
      },
    },
  ];
  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Data Brosur</h3>
        <NavLink to="/app/brosur/form-brosur">
          <button className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]">
            Tambah Data
          </button>
        </NavLink>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10 }}
        scroll={{ y: 400 }}
        loading={loading}
      />

      <Modal
        title="View Brosur"
        open={openModal}
        onOk={onCloseModal}
        onCancel={onCloseModal}
        footer={null}
        width={1000}
      >
        <div className="py-10">
          <iframe
            src={`${import.meta.env.VITE_BASE_URL_API}${documentActive}`}
            frameBorder={0}
            width={'100%'}
            height={500}
          ></iframe>
        </div>
      </Modal>
    </>
  );
};

export default index;
