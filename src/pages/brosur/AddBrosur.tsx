import { useState, useEffect } from 'react';
import { Form, Input, Button, Upload, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import type { UploadFile, UploadProps } from 'antd/es/upload/interface';
import axios from 'axios';

const AddBrosur = () => {
  const [kodebrosur, setKodeBrosur] = useState<string>();
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [fileList, setFileList] = useState<UploadFile[]>([]);

  const onFinish = async (values: any) => {
    try {
      let response = null;
      let payload = {
        kode_brosur: location.search ? kodebrosur : null,
        nama_brosur: values.nama_brosur,
        file: values.brosur_file ? values.brosur_file.file : null,
      };
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      };
      if (location.search) {
        response = await axios.put(
          `${import.meta.env.VITE_BASE_URL_API}brosur`,
          payload,
          config
        );
      } else {
        response = await axios.post(
          `${import.meta.env.VITE_BASE_URL_API}brosur`,
          payload,
          config
        );
      }

      if (response && response.data.statusCode === 422) {
        message.error(response.data.message);
      }

      if (
        response &&
        (response.data.statusCode === 201 || response.data.statusCode === 200)
      ) {
        message.success(response.data.message);
        navigate('/app/brosur');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getBrosurById = async (value: string) => {
    setKodeBrosur(value);
    const fetch = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}brosur/${value}`
    );

    if (fetch) {
      const { nama_brosur } = fetch.data.data;
      form.setFieldsValue({
        nama_brosur: nama_brosur,
      });
    }
  };

  useEffect(() => {
    if (location.search) {
      getBrosurById(location.search.split('=')[1]);
    }
  }, [location]);

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">
          {location.search ? 'Edit' : 'Tambah'} Data Brosur
        </h3>
      </div>

      <div className="w-[50%] mt-5">
        <Form
          form={form}
          name="employee_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Nama Brosur"
            name="nama_brosur"
            rules={[
              {
                required: true,
                message: 'Please input brosur name!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="File Brosur"
            name="brosur_file"
            rules={[
              {
                required: location.search ? false : true,
                message: 'Please input brosur file!',
              },
            ]}
          >
            <Upload {...props} accept=".pdf">
              <Button icon={<UploadOutlined />}>Select File</Button>
            </Upload>
          </Form.Item>
          <Form.Item>
            <div className="flex justify-end items-center w-full gap-3">
              <button
                type="reset"
                className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
              >
                Clear
              </button>
              <button
                type="submit"
                className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
              >
                {location.search ? 'Update' : 'Simpan'}
              </button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default AddBrosur;
