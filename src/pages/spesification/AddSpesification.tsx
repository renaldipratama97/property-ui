import { useEffect } from 'react';
import { Form, Input, message } from 'antd';
import axios from 'axios';
import { useNavigate, useLocation } from 'react-router-dom';

const AddSpesification = () => {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const location = useLocation();

  const onFinish = async (values: any) => {
    try {
      let response = null;
      let payload = {};

      if (location && location.search) {
        payload = {
          kode_spesifikasi: location.search.split('=')[1],
          nama_spesifikasi: values.nama_spesifikasi,
        };

        response = await axios.put(
          `${import.meta.env.VITE_BASE_URL_API}spesification`,
          payload
        );
      } else {
        payload = {
          nama_spesifikasi: values.nama_spesifikasi,
        };

        response = await axios.post(
          `${import.meta.env.VITE_BASE_URL_API}spesification`,
          payload
        );
      }

      if (
        response &&
        (response.data.statusCode === 201 || response.data.statusCode === 200)
      ) {
        message.success(response.data.message);
        navigate('/app/spesifikasi');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getSpesifikasiById = async (value: string) => {
    const fetch = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}spesification/${value}`
    );

    if (fetch) {
      const { nama_spesifikasi } = fetch.data.data;
      form.setFieldsValue({
        nama_spesifikasi: nama_spesifikasi,
      });
    }
  };

  useEffect(() => {
    if (location.search) {
      getSpesifikasiById(location.search.split('=')[1]);
    }
  }, [location]);

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">
          {location.search ? 'Edit' : 'Tambah'} Data Spesifikasi
        </h3>
      </div>

      <div className="w-[50%] mt-5">
        <Form
          form={form}
          name="employee_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Nama Spesifikasi"
            name="nama_spesifikasi"
            rules={[
              {
                required: true,
                message: 'Please input spesification name!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item>
            <div className="flex justify-end items-center w-full gap-3">
              <button
                type="reset"
                className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
              >
                Clear
              </button>
              <button
                type="submit"
                className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
              >
                {location.search ? 'Update' : 'Simpan'}
              </button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default AddSpesification;
