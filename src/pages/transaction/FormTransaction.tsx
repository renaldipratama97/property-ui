import { useState } from 'react';
import {
  Form,
  Input,
  Button,
  Upload,
  Select,
  UploadFile,
  UploadProps,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const FormTransaction = () => {
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [form] = Form.useForm();

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  return (
    <>
      <div className="w-[100%] mt-5">
        <Form
          form={form}
          name="property_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          // onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <div className="bg-[#00000025] w-full rounded-lg p-3 ">
            <div className="flex flex-col justify-between mb-5">
              <h3 className="text-[18px] font-bold">Form Transaksi</h3>
              <span className="text-[14px] font-light text-[#025595]">
                Please fill out the form below
              </span>
            </div>
            <div className="flex gap-5">
              <div className="w-6/12">
                <Form.Item
                  label="Type Pembayaran"
                  name="type_pembayaran"
                  rules={[
                    {
                      required: true,
                      message: 'Please input payment type!',
                    },
                  ]}
                >
                  <Select
                    placeholder="Select Type Pembayaran"
                    style={{ width: '100%' }}
                    size="large"
                    options={[
                      { value: 'Cash', label: 'Cash' },
                      { value: 'Credit', label: 'Credit' },
                    ]}
                  />
                </Form.Item>
                <Form.Item
                  label="Uang Muka (DP)"
                  name="uang_muka"
                  rules={[
                    {
                      required: true,
                      message: 'Please input first money!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="Tenor"
                  name="tenor"
                  rules={[
                    {
                      required: true,
                      message: 'Please select tenor!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="Bayar Perbulan"
                  name="bayar_perbulan"
                  rules={[
                    {
                      required: true,
                      message: 'Please input monthly payment!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="Total Bayar"
                  name="total_bayar"
                  rules={[
                    {
                      required: true,
                      message: 'Please input total payment!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
              </div>
              <div className="w-6/12">
                <Form.Item
                  label="N I K"
                  name="nik"
                  rules={[
                    {
                      required: true,
                      message: 'Please input nik!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="Nama Lengkap"
                  name="nama_lengkap"
                  rules={[
                    {
                      required: true,
                      message: 'Please input fullname!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="No Handphone"
                  name="no_handphone"
                  rules={[
                    {
                      required: true,
                      message: 'Please input phonenumber!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="Alamat Lengkap"
                  name="alamat"
                  rules={[
                    {
                      required: true,
                      message: 'Please input address!',
                    },
                  ]}
                >
                  <Input size="large" />
                </Form.Item>
                <Form.Item
                  label="File Pendukung"
                  name="file_pendukung"
                  rules={[
                    {
                      required: true,
                      message: 'Please input support file!',
                    },
                  ]}
                >
                  <Upload {...props} accept=".jpg, .jpeg, .png">
                    <Button icon={<UploadOutlined />}>Select File</Button>
                  </Upload>
                </Form.Item>
              </div>
            </div>
          </div>

          <Form.Item>
            <div className="flex justify-end items-center w-full gap-3 mt-5">
              <button
                type="reset"
                className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
              >
                Clear
              </button>
              <button
                type="submit"
                className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
              >
                Submit
              </button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default FormTransaction;
