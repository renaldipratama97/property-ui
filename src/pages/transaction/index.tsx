import { useEffect, useState } from 'react';
import axios from 'axios';
import { Skeleton, message } from 'antd';
import ShareIcon from '../../assets/img/share.svg';
import LocationIcon from '../../assets/img/location.svg';
import { Link } from 'react-router-dom';
import { BsFillCartFill } from 'react-icons/bs';

const index = () => {
  const [loading, setLoading] = useState(false);
  const [property, setProperty] = useState<any>([]);
  const [originFacility, setOriginFacility] = useState<any>([]);

  const getProperties = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}property`
      );

      if (response && response.data.statusCode === 200) {
        return response.data.data;
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getFacilities = async () => {
    try {
      const facilityFetch = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}facilities`
      );

      if (facilityFetch && facilityFetch.data.statusCode === 200) {
        return facilityFetch.data.data;
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  useEffect(() => {
    setLoading(true);
    Promise.all([getProperties(), getFacilities()]).then((res) => {
      setProperty(res[0]);
      setOriginFacility(res[1]);
      setLoading(false);
    });
  }, []);

  return (
    <>
      {!loading ? (
        <div>
          <div className="flex justify-between items-center">
            <h3 className="text-[18px] font-bold">Data Property</h3>
          </div>

          <div className="mt-10 grid grid-cols-4 auto-rows-[320px] gap-2">
            {property.length &&
              property.map((item: any, idx: number) => (
                <div
                  className="relative group border border-[#025595] rounded-xl p-2 cursor-pointer hover:bg-gray-300"
                  key={idx}
                >
                  <div className="absolute transition-all ease-in-out duration-500 opacity-0 group-hover:opacity-100 z-50 w-full h-full top-0 left-0 flex flex-col gap-3 justify-center items-center">
                    {/* <Link
                      to={`/app/form-transaksi-jual/${item.property.kode_property}`}
                    >
                      <button className="flex justify-center items-center gap-2 w-24 py-1 bg-white hover:bg-gray-200 border border-white hover:border-[#023f95] text-[#023f95] rounded-md">
                        <BsFillCartFill /> Jual
                      </button>
                    </Link> */}
                    <Link
                      to={`/app/form-transaksi-sewa/${item.property.kode_property}`}
                    >
                      <button className="flex justify-center items-center gap-2 w-24 py-1 bg-white hover:bg-gray-200 border border-white hover:border-[#023f95] text-[#023f95] rounded-md">
                        <BsFillCartFill /> Sewa
                      </button>
                    </Link>
                  </div>
                  <div className="w-100 rounded-xl h-[150px] group overflow-hidden">
                    <img
                      src={
                        import.meta.env.VITE_BASE_URL_API +
                        item.picture[0].url_picture
                      }
                      alt={`property-image-${idx}`}
                      className="w-[100%] h-[100%] object-cover rounded-xl transition-all duration-[1s] ease-out group-hover:scale-[1.03]"
                    />
                  </div>
                  <div className="flex items-center justify-between mt-2">
                    <div className="text-[#025595] text-[18px] leading-[20px] font-semibold">
                      Rp {item.property.harga_property.toLocaleString('en-US')}
                    </div>
                    <div className="w-5 h-5 flex justify-center items-center bg-[#02559545] rounded-md cursor-pointer">
                      <img
                        src={ShareIcon}
                        alt="share-icon"
                        width={10}
                        height={10}
                      />
                    </div>
                  </div>
                  <div className="font-bold text-black text-[18px] leading-[20px] mt-2">
                    {item.property.nama_property}
                  </div>
                  {/* <div className="flex gap-4 mt-5">
                    <div className="flex gap-1">
                      <img
                        src={Icon2}
                        alt="bed-facility"
                        width={15}
                        height={15}
                      />
                      <span className="text-[14px]">3</span>
                    </div>
                    <div className="flex gap-1">
                      <img
                        src={Icon1}
                        alt="bed-facility"
                        width={15}
                        height={15}
                      />
                      <span className="text-[14px]">3</span>
                    </div>
                    <div className="flex gap-1">
                      <img
                        src={Icon3}
                        alt="bed-facility"
                        width={15}
                        height={15}
                      />
                      <span className="text-[14px]">3</span>
                    </div>
                  </div> */}
                  <div className="flex items-start gap-3 mt-14">
                    <img
                      src={LocationIcon}
                      alt="location-icon"
                      width={20}
                      height={20}
                    />
                    <span className="text-[14px] leading-[18px] text-[#16100D]">
                      {item.property.alamat.length > 50
                        ? item.property.alamat.substring(0, 50) + '...'
                        : item.property.alamat.length}
                    </span>
                  </div>
                </div>
              ))}
          </div>
        </div>
      ) : (
        <Skeleton />
      )}
    </>
  );
};

export default index;
