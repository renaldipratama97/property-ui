import { useState, useEffect } from 'react';
import {
  Form,
  Input,
  Button,
  Upload,
  Select,
  UploadFile,
  UploadProps,
  InputNumber,
  message,
  Skeleton,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import LocationIcon from '../../assets/img/location.svg';
import { useNavigate } from 'react-router-dom';
import {
  getNextMonthDate,
  getThreeMonthsLaterDate,
  getSixMonthsLaterDate,
  getOneYearLaterDate,
} from '../../utils/helper';

interface propertyDto {
  alamat: string;
  createdAt: string;
  harga_property: number;
  harga_sewa_property: number;
  kode_property: string;
  latitude: string;
  longitude: string;
  nama_property: string;
  stok: number;
  type_property: string;
  updatedAt: number;
}

interface propertyPicture {
  createdAt: string;
  kode_property: string;
  kode_property_picture: string;
  updatedAt: string;
  url_picture: string;
}

const FormRentTransaction = () => {
  const [property, setProperty] = useState<propertyDto>();
  const [propertyPicture, setPropertyPicture] = useState<propertyPicture[]>();
  const [employeeCode, setEmployeeCode] = useState<any>();
  const [loading, setLoading] = useState(false);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [form] = Form.useForm();
  const { property_id } = useParams();
  const navigate = useNavigate();

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  const getPropertyById = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}property/${property_id}`
      );

      if (response && response.data.statusCode === 200) {
        setProperty(response.data.data.property);
        setPropertyPicture(response.data.data.picture);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const getDueDate = (value: string) => {
    if (value === 'Perbulan') {
      return getNextMonthDate();
    } else if (value === 'Per 3 Bulan') {
      return getThreeMonthsLaterDate();
    } else if (value === 'Per 6 Bulan') {
      return getSixMonthsLaterDate();
    } else {
      return getOneYearLaterDate();
    }
  };

  const onFinish = async (values: any) => {
    setLoading(true);
    try {
      const formData = new FormData();
      let response = null;

      const getDate = getDueDate(values.jenis_pembayaran);
      const inputDate = new Date(getDate);
      const year = inputDate.getFullYear();
      const month = String(inputDate.getMonth() + 1).padStart(2, '0');
      const day = String(inputDate.getDate()).padStart(2, '0');
      const dueDate = `${year}-${month}-${day}`;

      formData.append(
        'kode_property',
        property?.kode_property ? property.kode_property : ''
      );
      formData.append('jenis_pembayaran', values.jenis_pembayaran);
      formData.append('jatuh_tempo', dueDate);
      formData.append('total_bayar', values.total_bayar);
      formData.append('nik', values.nik);
      formData.append('nama_lengkap', values.nama_lengkap);
      formData.append('email', values.email);
      formData.append('no_handphone', values.no_handphone);
      formData.append('alamat_lengkap', values.alamat);
      formData.append('kode_employee', employeeCode);
      formData.append('file', values.file_pendukung.file);

      response = await axios.post(
        `${import.meta.env.VITE_BASE_URL_API}transaksi-sewa`,
        formData
      );

      if (response && response.data.statusCode === 422) {
        message.error(response.data.message);
      }

      if (response && response.data.statusCode === 201) {
        message.success(response.data.message);
        navigate('/app/transaksi');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const onSelectJenisPembayaran = (val: string) => {
    if (val.toLowerCase() === 'perbulan') {
      form.setFieldsValue({
        total_bayar: property?.harga_sewa_property,
      });
    }

    if (val.toLowerCase() === 'per 3 bulan') {
      form.setFieldsValue({
        total_bayar:
          property?.harga_sewa_property && property.harga_sewa_property * 3,
      });
    }

    if (val.toLowerCase() === 'per 6 bulan') {
      form.setFieldsValue({
        total_bayar:
          property?.harga_sewa_property && property.harga_sewa_property * 6,
      });
    }

    if (val.toLowerCase() === 'pertahun') {
      form.setFieldsValue({
        total_bayar:
          property?.harga_sewa_property && property.harga_sewa_property * 12,
      });
    }
  };

  useEffect(() => {
    getPropertyById();
    setEmployeeCode(localStorage.getItem('employee'));
  }, []);

  return (
    <>
      {!loading ? (
        <div className="w-[100%] mt-5">
          <Form
            form={form}
            name="property_form"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
          >
            <div className="bg-[#00000025] w-full rounded-lg p-3 ">
              <div className="flex flex-col justify-between mb-5">
                <h3 className="text-[18px] font-bold">Form Transaksi</h3>
                <span className="text-[14px] font-light text-[#025595]">
                  Please fill out the form below
                </span>
              </div>
              <div className="flex gap-5">
                <div className="w-6/12">
                  <div className="w-full h-[170px] flex gap-1 p-2 rounded-lg bg-white">
                    <div className="w-[35%] h-full">
                      <img
                        src={`${import.meta.env.VITE_BASE_URL_API}${
                          propertyPicture?.length &&
                          propertyPicture[0].url_picture
                        }`}
                        alt="property-image"
                        className="object-cover w-full h-full rounded-lg"
                      />
                    </div>
                    <div className="w-[65%] h-full">
                      <div className="font-bold text-black text-[18px]">
                        {property?.nama_property}
                      </div>
                      <div className="flex items-center justify-between">
                        <div className="text-[#025595] text-[14px] font-semibold">
                          Rp{' '}
                          {property?.harga_sewa_property
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                        </div>
                      </div>
                      <div className="flex items-start gap-3 mt-14">
                        <img
                          src={LocationIcon}
                          alt="location-icon"
                          width={20}
                          height={20}
                        />
                        <span className="text-[14px] leading-[18px] text-[#16100D]">
                          {property?.alamat}
                        </span>
                      </div>
                    </div>
                  </div>
                  <Form.Item
                    label="Jenis Pembayaran"
                    name="jenis_pembayaran"
                    className="mt-[13px]"
                    rules={[
                      {
                        required: true,
                        message: 'Please input payment type!',
                      },
                    ]}
                  >
                    <Select
                      placeholder="Select Jenis Pembayaran"
                      style={{ width: '100%' }}
                      size="large"
                      onSelect={(val) => onSelectJenisPembayaran(val)}
                      options={[
                        { value: 'Perbulan', label: 'Perbulan' },
                        { value: 'Per 3 Bulan', label: 'Per 3 Bulan' },
                        { value: 'Per 6 Bulan', label: 'Per 6 Bulan' },
                        { value: 'Pertahun', label: 'Pertahun' },
                      ]}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Total Bayar"
                    name="total_bayar"
                    rules={[
                      {
                        required: true,
                        message: 'Please input total payment!',
                      },
                    ]}
                  >
                    <InputNumber
                      className="w-full"
                      size="large"
                      prefix="Rp"
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                      }
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      disabled
                    />
                  </Form.Item>
                </div>
                <div className="w-6/12">
                  <Form.Item
                    label="N I K"
                    name="nik"
                    rules={[
                      {
                        required: true,
                        message: 'Please input nik!',
                      },
                    ]}
                  >
                    <Input size="large" />
                  </Form.Item>
                  <Form.Item
                    label="Nama Lengkap"
                    name="nama_lengkap"
                    rules={[
                      {
                        required: true,
                        message: 'Please input fullname!',
                      },
                    ]}
                  >
                    <Input size="large" />
                  </Form.Item>
                  <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: 'Please input email!',
                      },
                    ]}
                  >
                    <Input size="large" />
                  </Form.Item>
                  <Form.Item
                    label="No Handphone"
                    name="no_handphone"
                    rules={[
                      {
                        required: true,
                        message: 'Please input phonenumber!',
                      },
                    ]}
                  >
                    <Input size="large" />
                  </Form.Item>
                  <Form.Item
                    label="Alamat Lengkap"
                    name="alamat"
                    rules={[
                      {
                        required: true,
                        message: 'Please input address!',
                      },
                    ]}
                  >
                    <Input size="large" />
                  </Form.Item>
                  <Form.Item
                    label="File Pendukung"
                    name="file_pendukung"
                    rules={[
                      {
                        required: true,
                        message: 'Please input support file!',
                      },
                    ]}
                  >
                    <Upload {...props} accept=".pdf">
                      <Button icon={<UploadOutlined />}>Select File</Button>
                    </Upload>
                  </Form.Item>
                </div>
              </div>
            </div>

            <Form.Item>
              <div className="flex justify-end items-center w-full gap-3 mt-5">
                <button
                  type="reset"
                  className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
                >
                  Clear
                </button>
                <button
                  type="submit"
                  className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
                >
                  Submit
                </button>
              </div>
            </Form.Item>
          </Form>
        </div>
      ) : (
        <Skeleton />
      )}
    </>
  );
};

export default FormRentTransaction;
