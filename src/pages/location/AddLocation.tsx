import React, { useEffect, useState, useRef } from 'react';
import { Form, Input, message, Spin } from 'antd';
import axios from 'axios';
import { useNavigate, useLocation } from 'react-router-dom';
import {
  useJsApiLoader,
  GoogleMap,
  MarkerF,
  Autocomplete,
} from '@react-google-maps/api';

interface centerDto {
  kode_lokasi?: string;
  lat: number;
  lng: number;
  address: string;
  name: string;
  icon?: string;
}

const AddLocation = () => {
  const [searchResult, setSearchResult] = useState<any>(null);
  const [map, setMap] = useState(/** @type google.maps.Map */ null);
  const [center, setCenter] = useState<centerDto>({
    address:
      'Jl. Tilam, Rimba Panjang, Kec. Tambang, Kabupaten Kampar, Riau 28293',
    lat: 0.45854,
    lng: 101.3414875,
    name: '',
  });
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const location = useLocation();

  const originInput = useRef<HTMLInputElement>(null);

  const onFinish = async (values: any) => {
    try {
      let response;
      if (center.kode_lokasi) {
        response = await axios.put(
          `${import.meta.env.VITE_BASE_URL_API}location`,
          { ...values, kode_lokasi: center.kode_lokasi }
        );
      } else {
        response = await axios.post(
          `${import.meta.env.VITE_BASE_URL_API}location`,
          values
        );
      }
      if (
        response &&
        (response.data.statusCode === 201 || response.data.statusCode === 200)
      ) {
        message.success(response.data.message);
        navigate('/app/lokasi-terdekat');
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  function onLoad(autocomplete: any) {
    setSearchResult(autocomplete);
  }

  function onPlaceChanged() {
    if (searchResult != null) {
      const place = searchResult.getPlace();
      const { lat, lng } = place.geometry.location;
      setCenter({
        ...center,
        lat: lat(),
        lng: lng(),
        address: place.formatted_address,
        name: place.name,
        icon: place.icon,
      });
    } else {
      message.error('Please enter location');
    }
  }

  const getLocationById = async (id: string) => {
    const response = await axios.get(
      `${import.meta.env.VITE_BASE_URL_API}location/${id}`
    );

    if (response && response.data.statusCode === 200) {
      setCenter({
        kode_lokasi: response.data.data.kode_lokasi,
        address: response.data.data.alamat,
        lat: Number(response.data.data.latitude),
        lng: Number(response.data.data.longitude),
        name: response.data.data.nama_lokasi,
        icon: response.data.data.icon,
      });
    }
  };

  useEffect(() => {
    if (location.search) {
      getLocationById(location.search.split('=')[1]);
    }
  }, [location]);

  useEffect(() => {
    form.setFieldsValue({
      latitude: center.lat,
      longitude: center.lng,
      alamat: center.address,
      nama_lokasi: center.name,
      icon: center.icon,
    });
  }, [center]);

  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: import.meta.env.VITE_GMAPS_API_KEY,
    libraries: ['places'],
  });

  if (!isLoaded) {
    return (
      <div className="absolute w-full h-full flex justify-center items-center">
        <Spin style={{ fontSize: 50 }} />
      </div>
    );
  }

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">
          {location.search ? 'Edit' : 'Tambah'} Data Lokasi
        </h3>
      </div>

      <div className="flex gap-2">
        <div className="w-[50%] mt-5">
          <Form
            form={form}
            name="employee_form"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Nama Lokasi"
              name="nama_lokasi"
              rules={[
                {
                  required: true,
                  message: 'Please input location name!',
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="Latitude"
              name="latitude"
              rules={[
                {
                  required: true,
                  message: 'Please input latitude!',
                },
              ]}
            >
              <Input size="large" disabled />
            </Form.Item>
            <Form.Item
              label="Longitude"
              name="longitude"
              rules={[
                {
                  required: true,
                  message: 'Please input longitude!',
                },
              ]}
            >
              <Input size="large" disabled />
            </Form.Item>
            <Form.Item
              label="Alamat Lengkap"
              name="alamat"
              rules={[
                {
                  required: true,
                  message: 'Please input address!',
                },
              ]}
            >
              <Input size="large" disabled />
            </Form.Item>
            <Form.Item
              label="Icon"
              name="icon"
              rules={[
                {
                  required: true,
                  message: 'Please input icon!',
                },
              ]}
            >
              <Input size="large" disabled />
            </Form.Item>
            <Form.Item>
              <div className="flex justify-start items-center w-full gap-3">
                <button
                  type="reset"
                  className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
                >
                  Clear
                </button>
                <button
                  type="submit"
                  className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
                >
                  {center.kode_lokasi ? 'Update' : 'Simpan'}
                </button>
              </div>
            </Form.Item>
          </Form>
        </div>
        <div className="relative w-[50%] mt-5 h-[345px] rounded-md overflow-hidden">
          <div className="absolute z-20 w-full top-3 left-0 flex justify-center items-center">
            <Autocomplete
              onPlaceChanged={onPlaceChanged}
              onLoad={onLoad}
              className="w-full flex justify-center items-center"
            >
              <input
                type="text"
                className=" h-10 border border-[#025595] outline-none indent-2 w-[80%] rounded-md"
                ref={originInput}
              />
            </Autocomplete>
          </div>
          <GoogleMap
            center={center}
            zoom={15}
            mapContainerStyle={{ width: '100%', height: '100%' }}
            options={{
              zoomControl: false,
              streetViewControl: false,
              mapTypeControl: false,
              fullscreenControl: false,
            }}
            onLoad={(map: any) => setMap(map)}
          >
            <MarkerF position={center} />
          </GoogleMap>
        </div>
      </div>
    </>
  );
};

export default AddLocation;
