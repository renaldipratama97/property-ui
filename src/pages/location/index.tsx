import { useState, useEffect } from 'react';
import { Table, Button, message } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Link, NavLink } from 'react-router-dom';
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import axios from 'axios';

interface DataType {
  key: number;
  no: number;
  kode_lokasi: string;
  nama_lokasi: string;
  latitude: string;
  longitude: string;
  alamat: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState(false);

  const getLocations = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}locations`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_lokasi: val.kode_lokasi,
              nama_lokasi: val.nama_lokasi,
              latitude: val.latitude,
              longitude: val.longitude,
              alamat: val.alamat,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const deleteLocation = async (value: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `${import.meta.env.VITE_BASE_URL_API}location/${value}`
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getLocations();
    }
  };

  useEffect(() => {
    getLocations();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: 'No',
      dataIndex: 'no',
      width: '10%',
      align: 'center',
    },
    {
      title: 'Nama Lokasi',
      dataIndex: 'nama_lokasi',
      width: '20%',
      align: 'left',
    },
    {
      title: 'Latitude',
      dataIndex: 'latitude',
      width: '10%',
      align: 'center',
    },
    {
      title: 'Longitude',
      dataIndex: 'longitude',
      width: '10%',
      align: 'center',
    },
    {
      title: 'Alamat',
      dataIndex: 'alamat',
      width: '30%',
      align: 'left',
    },
    {
      title: 'Action',
      dataIndex: 'kode_lokasi',
      key: 'x',
      align: 'center',
      render: (value) => {
        return (
          <div className="flex gap-2 justify-center">
            <Link to={`/app/lokasi-terdekat/form-lokasi?kode=${value}`}>
              <Button type="primary" ghost className="btn-custom">
                <EditFilled />
              </Button>
            </Link>
            <Button
              type="primary"
              danger
              ghost
              onClick={() => deleteLocation(value)}
            >
              <DeleteFilled />
            </Button>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Data Lokasi</h3>
        <NavLink to="/app/lokasi-terdekat/form-lokasi">
          <button className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]">
            Tambah Data
          </button>
        </NavLink>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10 }}
        scroll={{ y: 400 }}
        loading={loading}
      />
    </>
  );
};

export default index;
