import { useState, useEffect } from 'react';
import { Table, Button, message, Tag } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { AiFillCheckCircle, AiFillCloseCircle } from 'react-icons/ai';
import axios from 'axios';
import { statusBooking } from '../../utils/constants';

interface DataType {
  key: number;
  no: number;
  kode_booking: string;
  kode_property: string;
  nik: string;
  nama_lengkap: string;
  email: string;
  no_handphone: string;
  alamat: string;
  tgl_akad: string;
  lokasi_akad: string;
  status: string;
}

const index = () => {
  const [data, setData] = useState<DataType[]>([]);
  const [loading, setLoading] = useState(false);

  const getBookings = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}bookings`
      );

      if (response && response.data.statusCode === 200) {
        let newData: DataType[] = [];
        if (response.data.data.length) {
          response.data.data.forEach((val: any, idx: number) => {
            const tempData = {
              key: idx,
              no: idx + 1,
              kode_booking: val.kode_booking,
              kode_property: val.kode_property,
              nik: val.nik,
              nama_lengkap: val.nama_lengkap,
              email: val.email,
              no_handphone: val.no_handphone,
              alamat: val.alamat,
              tgl_akad: val.tgl_akad,
              lokasi_akad: val.lokasi_akad,
              status: val.status,
            };
            newData.push(tempData);
          });
        }
        setData(newData);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
    }
  };

  const changeStatusBooking = async (
    kode_booking: string,
    kode_property: string,
    status: string
  ) => {
    setLoading(true);
    try {
      const response = await axios.put(
        `${import.meta.env.VITE_BASE_URL_API}booking`,
        { kode_booking, kode_property, status }
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }

      if (response && response.data.statusCode === 404) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      setLoading(false);
      getBookings();
    }
  };

  useEffect(() => {
    getBookings();
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: 'No',
      dataIndex: 'no',
      width: 60,
      align: 'center',
    },
    {
      title: 'Property',
      dataIndex: 'kode_property',
      width: '20%',
      align: 'center',
    },
    {
      title: 'Nama Lengkap',
      dataIndex: 'nama_lengkap',
      width: '15%',
      align: 'left',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      width: '15%',
      align: 'left',
    },
    {
      title: 'No Handphone',
      dataIndex: 'no_handphone',
      width: '15%',
      align: 'left',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      width: '15%',
      align: 'center',
      render: (value) => {
        return (
          <>
            {value === statusBooking.pending ? (
              <Tag color="orange">{value}</Tag>
            ) : value === statusBooking.terjual ? (
              <Tag color="green">{value}</Tag>
            ) : (
              <Tag color="red">{value}</Tag>
            )}
          </>
        );
      },
    },
    {
      title: 'Action',
      dataIndex: 'kode_booking',
      key: 'x',
      align: 'center',
      render: (value, row) => {
        return (
          <>
            {row.status === statusBooking.terjual ||
            row.status === statusBooking.gagal ? (
              ''
            ) : (
              <div className="flex gap-2 justify-center">
                <Button
                  type="primary"
                  ghost
                  className="btn-custom"
                  onClick={() =>
                    changeStatusBooking(
                      value,
                      row.kode_property,
                      statusBooking.terjual
                    )
                  }
                >
                  <AiFillCheckCircle className="text-base" />
                </Button>
                <Button
                  type="primary"
                  danger
                  ghost
                  onClick={() =>
                    changeStatusBooking(
                      value,
                      row.kode_property,
                      statusBooking.gagal
                    )
                  }
                >
                  <AiFillCloseCircle className="text-base" />
                </Button>
              </div>
            )}
          </>
        );
      },
    },
  ];

  return (
    <>
      <div className="flex justify-between items-center">
        <h3 className="text-[18px] font-bold">Data Booking</h3>
      </div>

      <Table
        className="mt-10"
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10 }}
        scroll={{ y: 400 }}
        loading={loading}
      />
    </>
  );
};

export default index;
