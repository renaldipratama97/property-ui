// import PropertyImage from '../../assets/img/image-1.svg';
// import Icon1 from '../../assets/img/icon1.svg';
// import Icon2 from '../../assets/img/icon2.svg';
// import Icon3 from '../../assets/img/icon3.svg';
import LocationIcon from '../../assets/img/location.svg';
import { AiFillEye } from 'react-icons/ai';
import { TbBrandBooking } from 'react-icons/tb';
import { useEffect, useState } from 'react';
import axios from 'axios';
import {
  message,
  Modal,
  Upload,
  UploadFile,
  UploadProps,
  Form,
  Input,
  Button,
  DatePicker,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import type { RangePickerProps } from 'antd/es/date-picker';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { Link } from 'react-router-dom';
dayjs.extend(customParseFormat);

interface selectedPropertyDto {
  kode_property: string;
  nama_property: string;
  type_property: string;
  harga_property: number;
  harga_sewa_property: number;
  latitude: string;
  longitude: string;
  stok: number;
  alamat: string;
  picture: string;
}

const PropertyCard = () => {
  const [property, setProperty] = useState<any>([]);
  const [selectedProperty, setSelectedProperty] =
    useState<selectedPropertyDto>();
  const [originFacility, setOriginFacility] = useState<any>([]);
  const [openModal, setOpenModal] = useState(false);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [form] = Form.useForm();

  const range = (start: number, end: number) => {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  };

  const disabledDate: RangePickerProps['disabledDate'] = (current) => {
    return current && current < dayjs().endOf('day');
  };

  const disabledDateTime = () => ({
    disabledHours: () => range(0, 8),
    disabledMinutes: () => range(30, 60),
    disabledSeconds: () => [55, 56],
  });

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  const getProperties = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}property`
      );

      if (response && response.data.statusCode === 200) {
        return response.data.data;
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const getFacilities = async () => {
    try {
      const facilityFetch = await axios.get(
        `${import.meta.env.VITE_BASE_URL_API}facilities`
      );

      if (facilityFetch && facilityFetch.data.statusCode === 200) {
        return facilityFetch.data.data;
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };

  const onOpenModal = (val: any) => {
    const { property, picture } = val;
    setSelectedProperty({
      kode_property: property.kode_property,
      nama_property: property.nama_property,
      type_property: property.type_property,
      harga_property: property.harga_property,
      harga_sewa_property: property.harga_sewa_property,
      latitude: property.latitude,
      longitude: property.longitude,
      stok: property.stok,
      alamat: property.alamat,
      picture: picture[0].url_picture,
    });
    form.setFieldsValue({
      lokasi_akad: property.alamat,
    });
    setOpenModal(!openModal);
  };

  const onFinish = async (values: any) => {
    try {
      const payload = {
        kode_property: selectedProperty?.kode_property,
        nik: values.nik,
        nama_lengkap: values.nama_lengkap,
        email: values.email,
        no_handphone: values.no_handphone,
        alamat: values.alamat,
        file: values.file_pendukung.file,
        tgl_akad: values.tgl_akad,
        lokasi_akad: values.lokasi_akad,
      };

      const response = await axios.post(
        `${import.meta.env.VITE_BASE_URL_API}booking`,
        payload,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        }
      );

      if (response && response.data.statusCode === 201) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed: ${error}`);
    } finally {
      setOpenModal(!openModal);
    }
  };

  useEffect(() => {
    Promise.all([getProperties(), getFacilities()]).then((res) => {
      setProperty(res[0]);
      setOriginFacility(res[1]);
    });
  }, []);

  return (
    <>
      <div className="w-12/12 px-[10%] py-[50px]">
        <h1 className="font-bold text-[40px] leading-[56px] my-0 mx-auto text-center">
          Our most trending properties
        </h1>
        <p className="text-center font-normal text-[16px] leading-[30px] font-syne text-[#6f6f6f]">
          It is a long established fact that a reader will be distracted by the
          readable <br />
          content of a page when looking at its layout. The point of using.
        </p>

        <div className="grid grid-cols-3 auto-rows-[500px] gap-5 mt-10">
          {property.length &&
            property.map((item: any, idx: number) => (
              <div
                className="group relative rounded-2xl p-3 bg-white transition-all duration-[1s] ease-out hover:scale-[1.03] overflow-hidden"
                key={idx}
              >
                <div className="absolute z-20 transition ease-in-out duration-500 opacity-0 group-hover:opacity-100 bg-[#00000024] rounded-tl-[50%] rounded-tr-[50%] w-full h-[30%] left-0 bottom-0 flex gap-2 justify-center items-center">
                  <Link to={`/detail-property/${item.property.kode_property}`}>
                    <button className="py-3 w-28 rounded-md bg-white gap-1 flex justify-center items-center border border-[#146C94] text-lg text-[#146C94]">
                      <AiFillEye className="text-xl text-[#146C94]" /> Detail
                    </button>
                  </Link>
                  <button
                    onClick={() => onOpenModal(item)}
                    className="py-3 w-28 rounded-md bg-white gap-1 flex justify-center items-center border border-[#146C94] text-lg text-[#146C94]"
                  >
                    <TbBrandBooking className="text-xl text-[#146C94]" />
                    Booking
                  </button>
                </div>
                <div className="w-100 rounded-xl h-[290px] group overflow-hidden">
                  <img
                    src={
                      import.meta.env.VITE_BASE_URL_API +
                      item.picture[0].url_picture
                    }
                    alt="property-image"
                    className="w-[100%] h-[100%] object-cover rounded-xl transition-all duration-[1s] ease-out group-hover:scale-[1.03]"
                  />
                </div>
                <div className="flex items-center justify-between mt-5">
                  <div className="text-[#025595] text-[25px] leading-[28px] font-bold">
                    Rp {item.property.harga_property.toLocaleString('en-US')}
                  </div>
                </div>
                <div className="font-bold text-black text-[24px] leading-[28px] mt-5 h-20">
                  {item.property.nama_property}
                </div>
                {/* <div className="flex gap-7">
                  <div className="flex gap-2">
                    <img src={Icon2} alt="bed-facility" />
                    <span>3</span>
                  </div>
                  <div className="flex gap-2">
                    <img src={Icon1} alt="bed-facility" />
                    <span>3</span>
                  </div>
                  <div className="flex gap-2">
                    <img src={Icon3} alt="bed-facility" />
                    <span>3</span>
                  </div>
                </div> */}
                <div className="flex gap-3">
                  <img src={LocationIcon} alt="location-icon" />
                  <span className="text-[16px] leading-[20px] text-[#16100D]">
                    {item.property.alamat}
                  </span>
                </div>
              </div>
            ))}
        </div>
      </div>

      <Modal
        open={openModal}
        footer={false}
        closable={false}
        onCancel={() => setOpenModal(!openModal)}
        width={700}
      >
        <div className="w-full text-center">
          <h1 className="text-2xl font-bold">FORM BOOKING</h1>
        </div>

        <Form
          form={form}
          name="employee_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="N I K"
            name="nik"
            rules={[
              {
                required: true,
                message: 'Please input nik!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="Nama Lengkap"
            name="nama_lengkap"
            rules={[
              {
                required: true,
                message: 'Please input fullname!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input email!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="No Handphone"
            name="no_handphone"
            rules={[
              {
                required: true,
                message: 'Please input phonenumber!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="Alamat Lengkap"
            name="alamat"
            rules={[
              {
                required: true,
                message: 'Please input address!',
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="File Pendukung (KTP/KK)"
            name="file_pendukung"
            rules={[
              {
                required: true,
                message: 'Please input support file!',
              },
            ]}
          >
            <Upload {...props} accept=".pdf">
              <Button icon={<UploadOutlined />}>Select File</Button>
            </Upload>
          </Form.Item>
          <Form.Item
            label="Tanggal Transaksi"
            name="tgl_akad"
            rules={[
              {
                required: true,
                message: 'Please input date of transaction!',
              },
            ]}
          >
            <DatePicker
              style={{ width: '100%' }}
              size="large"
              disabledDate={disabledDate}
              disabledTime={disabledDateTime}
              format="YYYY-MM-DD HH:mm:ss"
              showTime={{ defaultValue: dayjs('08:00:00', 'HH:mm:ss') }}
            />
          </Form.Item>
          <Form.Item label="Lokasi Transaksi" name="lokasi_akad">
            <Input size="large" />
          </Form.Item>
          <Form.Item>
            <div className="flex justify-end items-center w-full gap-3">
              <button
                type="reset"
                className="bg-white border border-[#025595] hover:bg-[#f2f2f2] px-8 py-2 text-[#025595] rounded-xl text-[12px]"
              >
                Clear
              </button>
              <button
                type="submit"
                className="bg-[#025595] hover:bg-[#023f95] px-8 py-2 text-gray-200 rounded-xl text-[12px]"
              >
                Simpan
              </button>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default PropertyCard;
