import { BsFacebook, BsLinkedin, BsTwitter } from 'react-icons/bs';
import { Link } from 'react-router-dom';

export default function BaseFooter() {
  return (
    <>
      <div className="w-12/12 px-[10%] bg-[#ffffff70] text-black mt-20">
        <div className="py-[20px] grid grid-cols-5 gap-3 grid-rows-[200px]">
          <div className="col-span-2">
            <Link to="/">
              <div className="flex flex-col justify-start gap-2">
                <img
                  src="/logo.svg"
                  alt="icon-aricha-propertindo"
                  height={30}
                  width={50}
                />
                <span className="text-[20px] font-bold text-[#145094]">
                  PT. Aricha Propertindo
                </span>
              </div>
            </Link>
            <div className="mt-5">
              Jl. Tilam, Rimba Panjang, Kec. Tambang, Kabupaten Kampar, Riau
              28293
            </div>
          </div>
          <div>
            <h1 className="font-bold text-2xl">Service</h1>
            <ul className="mt-5 font-thin text-base flex flex-col gap-2">
              <li className="cursor-pointer hover:text-[#146C94]">
                Payment & Tax
              </li>
              <li className="cursor-pointer hover:text-[#146C94]">Features</li>
              <li className="cursor-pointer hover:text-[#146C94]">
                View Booking
              </li>
              <li className="cursor-pointer hover:text-[#146C94]">Support</li>
            </ul>
          </div>
          <div>
            <h1 className="font-bold text-2xl">About</h1>
            <ul className="mt-5 font-thin text-base flex flex-col gap-2">
              <li className="cursor-pointer hover:text-[#146C94]">About Us</li>
              <li className="cursor-pointer hover:text-[#146C94]">News</li>
              <li className="cursor-pointer hover:text-[#146C94]">Pricing</li>
              <li className="cursor-pointer hover:text-[#146C94]">
                New Property
              </li>
            </ul>
          </div>
          <div>
            <h1 className="font-bold text-2xl">Our Social Media</h1>
            <div className="flex gap-2 mt-5">
              <div className="w-12 h-12 rounded-[50%] bg-[#146C94] text-white flex justify-center items-center cursor-pointer">
                <BsFacebook className="text-[100%]" />
              </div>
              <div className="w-12 h-12 rounded-[50%] bg-[#146C94] text-white flex justify-center items-center cursor-pointer">
                <BsLinkedin className="text-[100%]" />
              </div>
              <div className="w-12 h-12 rounded-[50%] bg-[#146C94] text-white flex justify-center items-center cursor-pointer">
                <BsTwitter className="text-[100%]" />
              </div>
            </div>
          </div>
        </div>
        <div className="py-[15px] border-t border-black w-full flex items-center justify-between ">
          <div className="text-base font-semibold cursor-pointer">
            Copyright 2023 PT. Aricha Propertindo.
          </div>
          <div className="flex gap-5 justify-center items-center text-base font-semibold">
            <span className="cursor-pointer">Terms & Conditions</span>
            <span className="cursor-pointer">Privacy Policy</span>
          </div>
        </div>
      </div>
    </>
  );
}
