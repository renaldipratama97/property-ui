import { Link } from 'react-router-dom';

const BaseHeader = (props: any) => {
  return (
    <>
      <div className="fixed z-[1000] top-0 w-full h-[90px] bg-[#ffffff20] rounded-b-lg px-[10%] flex justify-between items-center">
        <Link to="/">
          <div className="flex flex-col items-center max-w-max cursor-pointer">
            <img src="/logo.svg" alt="logo" height={30} width={50} />
            <span className="text-[20px] font-bold text-[#145094]">
              Aricha Propertindo
            </span>
          </div>
        </Link>
        <div className="flex gap-3">
          <button
            onClick={() => props.setOpenModal(!props.value)}
            className="px-10 py-3 rounded-2xl text-[#146C94] bg-[#F6F1F1] font-bold border border-[#146C94]"
          >
            Brosur
          </button>
          <Link to="/auth/login">
            <button className="px-10 py-3 rounded-2xl text-[#F6F1F1] bg-[#146C94] font-bold">
              Login
            </button>
          </Link>
        </div>
      </div>
    </>
  );
};

export default BaseHeader;
