import { menuAdmin, menuDirektur, menuSales } from '../../utils/menu';
import { NavLink } from 'react-router-dom';

const Sidebar = () => {
  const activeLink =
    'flex items-center gap-5 pl-4 pt-3 pb-2.5 rounded-lg text-gray-200 text-md m-2';
  const normalLink =
    'flex items-center gap-5 pl-4 pt-3 pb-2.5 rounded-lg text-md text-gray-200 dark:text-gray-200 dark:hover:text-gray-200 hover:bg-[#025595] m-2';

  return (
    <>
      <div className="w-[20%] min-h-screen bg-[#33373E] flex flex-col">
        <div className="flex justify-center items-center flex-col mt-10">
          <div className="cursor-pointer w-20">
            <img
              src="/logo.svg"
              alt="logo-icon"
              className="object-cover w-full"
            />
          </div>
          <h1 className="cursor-pointer text-[22px] font-bold font-syne text-gray-200 mt-3">
            PT Aricha Propertindo
          </h1>
        </div>

        <div className="mt-5 w-full">
          {localStorage.getItem('user_level') === 'Sales'
            ? menuSales.map((item) => (
                <div key={item.title}>
                  <p className="text-gray-400 dark:text-gray-400 m-3 mt-4 uppercase">
                    {item.title}
                  </p>
                  {item.links.map((link) => (
                    <NavLink
                      to={`/app/${link.name}`}
                      key={link.name}
                      // onClick={handleCloseSideBar}
                      // style={({ isActive }) => ({
                      //   backgroundColor: isActive ? "blue" : "",
                      // })}
                      className={({ isActive }) =>
                        isActive ? activeLink : normalLink
                      }
                    >
                      {link.icon}
                      <span className="capitalize">
                        {link.name.toLowerCase() === 'transaksi'
                          ? 'Sewa Property'
                          : link.name.toLowerCase() === 'laporan-transaksi'
                          ? 'Laporan Sewa Property'
                          : link.name.replaceAll('-', ' ')}
                      </span>
                    </NavLink>
                  ))}
                </div>
              ))
            : localStorage.getItem('user_level') === 'Direktur'
            ? menuDirektur.map((item) => (
                <div key={item.title}>
                  <p className="text-gray-400 dark:text-gray-400 m-3 mt-4 uppercase">
                    {item.title}
                  </p>
                  {item.links.map((link) => (
                    <NavLink
                      to={`/app/${link.name}`}
                      key={link.name}
                      // onClick={handleCloseSideBar}
                      // style={({ isActive }) => ({
                      //   backgroundColor: isActive ? "blue" : "",
                      // })}
                      className={({ isActive }) =>
                        isActive ? activeLink : normalLink
                      }
                    >
                      {link.icon}
                      <span className="capitalize">
                        {link.name.toLowerCase() === 'transaksi'
                          ? 'Sewa Property'
                          : link.name.toLowerCase() === 'laporan-transaksi'
                          ? 'Laporan Sewa Property'
                          : link.name.replaceAll('-', ' ')}
                      </span>
                    </NavLink>
                  ))}
                </div>
              ))
            : menuAdmin.map((item) => (
                <div key={item.title}>
                  <p className="text-gray-400 dark:text-gray-400 m-3 mt-4 uppercase">
                    {item.title}
                  </p>
                  {item.links.map((link) => (
                    <NavLink
                      to={`/app/${link.name}`}
                      key={link.name}
                      // onClick={handleCloseSideBar}
                      // style={({ isActive }) => ({
                      //   backgroundColor: isActive ? "blue" : "",
                      // })}
                      className={({ isActive }) =>
                        isActive ? activeLink : normalLink
                      }
                    >
                      {link.icon}
                      <span className="capitalize">
                        {link.name.toLowerCase() === 'transaksi'
                          ? 'Sewa Property'
                          : link.name.toLowerCase() === 'laporan-transaksi'
                          ? 'Laporan Sewa Property'
                          : link.name.replaceAll('-', ' ')}
                      </span>
                    </NavLink>
                  ))}
                </div>
              ))}
        </div>
      </div>
    </>
  );
};

export default Sidebar;
