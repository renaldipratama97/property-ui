import Sidebar from "./Sidebar";
import Header from "./Header";

const index = (props: any) => {
  return (
    <>
      <div className="w-12/12 flex bg-[#20232A]">
        {/* Sidebar */}
        <Sidebar />
        <div className="w-[80%] min-h-screen relative">
          {/* Header */}
          <Header />

          {/* content */}
          <div className="w-[100%] min-h-[500px] mt-[110px] p-5">
            <div className="bg-gray-200 w-full min-h-[500px] rounded-2xl p-5">
              {props.children}
            </div>
          </div>

          {/* footer */}
          {/* <div></div> */}
        </div>
      </div>
    </>
  );
};

export default index;
