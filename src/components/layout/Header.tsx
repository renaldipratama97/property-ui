import { useState } from "react";
import { Popover } from "antd";
import { IoMenu } from "react-icons/io5";
import { ImProfile } from "react-icons/im";
import { AiOutlineLogout } from "react-icons/ai";
import { useSignOut } from "react-auth-kit";
import { useNavigate } from "react-router-dom";

const profileMenu = () => {
  const signOut = useSignOut();
  const navigate = useNavigate();

  const handleSignOut = () => {
    signOut();
    localStorage.clear();
    navigate("/");
  };

  return (
    <ul className="w-[150px] h-auto bg-white rounded-md py-1">
      <li className="flex items-center font-syne gap-2 hover:bg-[#025595] text-[15px] hover:text-gray-200 hover:font-semibold px-2 py-2 rounded-md cursor-pointer">
        <ImProfile /> <span>Profile</span>
      </li>
      <li
        className="flex items-center font-syne gap-2 hover:bg-[#025595] text-[15px] hover:text-gray-200 hover:font-semibold px-2 py-2 rounded-md cursor-pointer"
        onClick={() => handleSignOut()}
      >
        <AiOutlineLogout /> <span>Sign Out</span>
      </li>
    </ul>
  );
};

const Header = () => {
  const [open, setOpen] = useState(false);

  const hide = () => {
    setOpen(false);
  };

  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
  };

  return (
    <div className="flex items-center justify-between px-3 fixed top-0 h-[60px] bg-[#20232A] w-[80%] z-[1000]">
      <div className="w-14 h-[80%] cursor-pointer hover:bg-[#025595] flex items-center justify-center rounded-3xl">
        <IoMenu className="text-[25px] text-gray-200" />
      </div>
      <div className="flex gap-5">
        {/* <div className="for-flag w-[60px] h-[40px] bg-white cursor-pointer"></div> */}
        <Popover
          content={profileMenu}
          title={false}
          trigger="click"
          open={open}
          onOpenChange={handleOpenChange}
        >
          <div className="w-[40px] h-[40px] rounded-[50%] bg-white cursor-pointer">
            <img
              src={`${import.meta.env.VITE_BASE_URL_API}${localStorage.getItem(
                "picture"
              )}`}
              alt="img-user"
              className="object-cover rounded-[50%]"
            />
          </div>
        </Popover>
      </div>
    </div>
  );
};

export default Header;
