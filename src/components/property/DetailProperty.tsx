import { Skeleton, Tag } from 'antd';
import LocationIcon from '../../assets/img/location.svg';
import Icon1 from '../../assets/img/icon1.svg';
import Icon2 from '../../assets/img/icon2.svg';
import Icon3 from '../../assets/img/icon3.svg';
import {
  propertyDto,
  facilityPropertyDto,
  locationDto,
  spesificationPropertyDto,
} from '../../utils/interface';
import { _getDistanceFromLatLonInKm } from '../../utils/helper';
import { GoogleMap, useJsApiLoader, MarkerF } from '@react-google-maps/api';
import { useState, useEffect } from 'react';

type Props = {
  property?: propertyDto;
  facility?: facilityPropertyDto[];
  location?: locationDto[];
  spesification?: spesificationPropertyDto[];
};

const defaultCenter = {
  address:
    'Jl. Tilam, Rimba Panjang, Kec. Tambang, Kabupaten Kampar, Riau 28293',
  lat: 0.45854,
  lng: 101.3414875,
};

export default function DetailProperty({
  property,
  facility,
  location,
  spesification,
}: Props) {
  const [center, setCenter] = useState(defaultCenter);

  useEffect(() => {
    if (property) {
      setCenter({ ...center, lat: property.latitude, lng: property.longitude });
    }
  }, [property]);

  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: import.meta.env.VITE_GMAPS_API_KEY,
    libraries: ['places'],
  });

  if (!isLoaded) {
    return <Skeleton />;
  }

  return (
    <>
      <div className="w-full px-[10%] py-[50px] mt-20">
        <div className="w-full flex gap-5">
          <div className="w-5/12 h-[300px] rounded-md overflow-hidden">
            <img
              src={`${import.meta.env.VITE_BASE_URL_API}${property?.picture}`}
              alt="image-property"
              className="w-full h-full object-cover"
            />
          </div>
          <div className="w-7/12 h-[300px] flex flex-col justify-between">
            <div className="name and currency">
              <div className="font-bold text-black text-[40px] leading-[28px]">
                {property?.nama_property}
              </div>
              <div className="text-[#025595] text-[25px] leading-[28px] font-bold mt-5 flex gap-2 items-center">
                Rp {property?.harga_property.toLocaleString('en-US')}
                <Tag color="blue" className="text-[8px]">
                  Jual
                </Tag>
              </div>
              <div className="text-[#025595] text-[25px] leading-[28px] font-bold flex gap-2 items-center">
                Rp {property?.harga_sewa_property.toLocaleString('en-US')}
                <Tag color="magenta" className="text-[8px]">
                  Sewa
                </Tag>
              </div>
            </div>
            <div className="address and facility">
              <div className="w-full flex">
                <div>
                  <h1 className="mb-3 font-extrabold">Fasilitas : </h1>
                  <table className="text-[16px] leading-[20px] text-[#16100D]">
                    {facility?.map((val: any) => (
                      <tr key={val.kode_property_facility}>
                        <td className="w-32">{val.nama_fasilitas}</td>
                        <td className="w-5">:</td>
                        <td>{val.value}</td>
                      </tr>
                    ))}
                  </table>
                </div>
                <div className="ml-10">
                  <h1 className="mb-3 font-extrabold">Spesifikasi : </h1>
                  <table className="text-[16px] leading-[20px] text-[#16100D]">
                    {spesification?.map((val: any) => (
                      <tr key={val.kode_property_spesification}>
                        <td className="w-32">{val.nama_spesifikasi}</td>
                        <td className="w-5">:</td>
                        <td>{val.value}</td>
                      </tr>
                    ))}
                  </table>
                </div>
              </div>
              {/* <div className="flex gap-7">
                {facility?.map((val: any, idx: number) => (
                  <div className="flex gap-2" key={idx}>
                    <img
                      src={idx === 0 ? Icon1 : idx === 1 ? Icon2 : Icon3}
                      alt="icon-facility"
                    />
                    <span>{val.value}</span>
                  </div>
                ))}
              </div> */}
              <div className="flex gap-3 mt-5">
                <img src={LocationIcon} alt="location-icon" />
                <span className="text-[16px] leading-[20px] text-[#16100D]">
                  {property?.alamat}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-10 w-full px-[10%]">
        <h1 className="text-center text-3xl font-extrabold mb-5">
          Lokasi Property
        </h1>
        <div className="w-full h-[400px] border border-gray-300 rounded-lg overflow-hidden">
          <GoogleMap
            center={center}
            mapContainerClassName="wrapper-maps"
            zoom={15}
            mapContainerStyle={{
              width: '100%',
              height: '100%',
            }}
            options={{
              zoomControl: true,
              streetViewControl: false,
              mapTypeControl: false,
              fullscreenControl: false,
            }}
          >
            <MarkerF position={center} draggable={false} />
          </GoogleMap>
        </div>
      </div>
      <div className="mt-10 w-full px-[10%]">
        <h1 className="text-center text-3xl font-extrabold">Lokasi Terdekat</h1>
        <div className="w-full grid grid-cols-6 auto-rows-[200px] py-10 gap-3">
          {location?.map((val: locationDto) => (
            <div
              className="flex flex-col items-center rounded-md overflow-hidden"
              key={val.kode_lokasi}
            >
              <img
                src={val.icon}
                alt={`${val.nama_lokasi}`}
                className="w-[50%] h-20 object-contain text-center"
              />
              <h4 className="text-center text-sm mt-1">{val.nama_lokasi}</h4>
              <span className="font-bold text-center">
                (
                {_getDistanceFromLatLonInKm(
                  property?.latitude ? property.latitude : 0,
                  property?.longitude ? property.longitude : 0,
                  val.latitude,
                  val.longitude
                ).toFixed(2)}{' '}
                KM )
              </span>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
