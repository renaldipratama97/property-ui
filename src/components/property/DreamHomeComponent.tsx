import DreamHomeImage from "../../assets/img/dream-property.svg";
import Icon1 from "../../assets/img/icon1.svg";
import Icon2 from "../../assets/img/icon2.svg";
import Icon3 from "../../assets/img/icon3.svg";
import LocationIcon from "../../assets/img/location.svg";

const DreamHomeComponent = () => {
  return (
    <>
      <div className="w-full px-[10%] py-[50px] mt-20">
        <h1 className="font-bold text-[40px] leading-[56px] my-0 mx-auto text-center">
          We will help you to find <br />
          Dream Home
        </h1>
        <p className="text-center font-normal text-[16px] leading-[30px] font-syne text-[#6f6f6f]">
          It is a long established fact that a reader will be distracted by the
          readable content of a page <br />
          when looking at its layout. The point of using Lorem Ipsum is that it
          has a more-or-less normal distribution of letters
        </p>

        <div className="mt-10 flex flex-col gap-10 w-100">
          <div className="flex items-center relative w-100 h-[438px]">
            <div className="h-[215px] w-[275px] z-30 bg-white/90 rounded-2xl p-[20px]">
              <span className="text-[16px] leading-[19px] text-[#16100D]">
                $2000/month
              </span>
              <h3 className="mt-[30px] font-bold text-[18px] leading-[22px] text-[#16100D]">
                Apartment 2500 sqft
              </h3>
              <div className="flex gap-4 mt-5">
                <div className="flex gap-2">
                  <img src={Icon2} height={20} width={20} alt="bed-facility" />
                  <span>3</span>
                </div>
                <div className="flex gap-2">
                  <img src={Icon1} height={20} width={20} alt="bed-facility" />
                  <span>3</span>
                </div>
                <div className="flex gap-2">
                  <img src={Icon3} height={20} width={20} alt="bed-facility" />
                  <span>3</span>
                </div>
              </div>
              <div className="flex items-start gap-3 mt-3">
                <img src={LocationIcon} alt="location-icon" />
                <span className="text-[14px] leading-[20px] font-normal text-[#16100D]">
                  1901 Thornridge Cir. Shiloh, Hawaii 81063
                </span>
              </div>
            </div>
            <div className="absolute w-[88%] h-full right-0 rounded-2xl">
              <img
                src={DreamHomeImage}
                alt="dream-home-image"
                className="w-full h-full object-cover rounded-2xl"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DreamHomeComponent;
