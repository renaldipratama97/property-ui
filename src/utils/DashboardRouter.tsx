import Layout from '../components/layout/index';
import { Routes, Route } from 'react-router-dom';
import Dashboard from '../pages/dashboard/index';
import Fasilitas from '../pages/facility/index';
import AddFacility from '../pages/facility/AddFacility';
import Spesifikasi from '../pages/spesification/index';
import AddSpesifikasi from '../pages/spesification/AddSpesification';
import Brosur from '../pages/brosur/index';
import AddBrosur from '../pages/brosur/AddBrosur';
import Employee from '../pages/employee/index';
import AddEmployee from '../pages/employee/AddEmployee';
import User from '../pages/user/index';
import AddUser from '../pages/user/AddUser';
import Property from '../pages/property/index';
import AddProperty from '../pages/property/AddProperty';
import Transaksi from '../pages/transaction';
import FormTransaction from '../pages/transaction/FormTransaction';
import FormRentTransaction from '../pages/transaction/FormRentTransaction';
import Location from '../pages/location/index';
import AddLocation from '../pages/location/AddLocation';
import Booking from '../pages/booking/index';
import Laporan from '../pages/laporan/index';
import ViewDetailProperty from '../pages/property/ViewProperty';

const DashboardRouter = () => {
  return (
    <>
      <Layout>
        <Routes>
          <Route path="/beranda" element={<Dashboard />} />
          <Route path="/fasilitas" element={<Fasilitas />} />
          <Route path="/fasilitas/form-fasilitas" element={<AddFacility />} />
          <Route path="/spesifikasi" element={<Spesifikasi />} />
          <Route
            path="/spesifikasi/form-spesifikasi"
            element={<AddSpesifikasi />}
          />
          <Route path="/brosur" element={<Brosur />} />
          <Route path="/brosur/form-brosur" element={<AddBrosur />} />
          <Route path="/karyawan" element={<Employee />} />
          <Route path="/karyawan/form-karyawan" element={<AddEmployee />} />
          <Route path="/property" element={<Property />} />
          <Route path="/property/form-property" element={<AddProperty />} />
          <Route
            path="/property/:kode_property"
            element={<ViewDetailProperty />}
          />
          <Route path="/booking" element={<Booking />} />
          <Route path="/transaksi" element={<Transaksi />} />
          <Route
            path="/form-transaksi-jual/:property_id"
            element={<FormTransaction />}
          />
          <Route
            path="/form-transaksi-sewa/:property_id"
            element={<FormRentTransaction />}
          />
          <Route path="/laporan-transaksi" element={<Laporan />} />
          <Route path="/user" element={<User />} />
          <Route path="/user/tambah-user" element={<AddUser />} />
          <Route path="/lokasi-terdekat" element={<Location />} />
          <Route
            path="/lokasi-terdekat/form-lokasi"
            element={<AddLocation />}
          />
        </Routes>
      </Layout>
    </>
  );
};

export default DashboardRouter;
