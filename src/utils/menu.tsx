import { TbTools, TbReportAnalytics } from 'react-icons/tb';
import { RiBuilding4Fill, RiFolderSettingsFill } from 'react-icons/ri';
import { IoLogoMicrosoft } from 'react-icons/io5';
import { FaUsers } from 'react-icons/fa';
import { BsFileImageFill } from 'react-icons/bs';
import { FaRegUserCircle, FaLocationArrow } from 'react-icons/fa';
import { TbReportMoney, TbBrandBooking } from 'react-icons/tb';

export const menuAdmin = [
  {
    title: 'Dashboard',
    links: [
      {
        name: 'beranda',
        icon: <IoLogoMicrosoft />,
      },
    ],
  },
  {
    title: 'Master',
    links: [
      {
        name: 'fasilitas',
        icon: <TbTools />,
      },
      {
        name: 'spesifikasi',
        icon: <RiFolderSettingsFill />,
      },
      {
        name: 'brosur',
        icon: <BsFileImageFill />,
      },
      {
        name: 'karyawan',
        icon: <FaUsers />,
      },
      {
        name: 'property',
        icon: <RiBuilding4Fill />,
      },
      {
        name: 'booking',
        icon: <TbBrandBooking />,
      },
    ],
  },
  {
    title: 'transaksi',
    links: [
      {
        name: 'transaksi',
        icon: <TbReportMoney />,
      },
    ],
  },
  {
    title: 'laporan',
    links: [
      {
        name: 'laporan-transaksi',
        icon: <TbReportAnalytics />,
      },
    ],
  },
  {
    title: 'settings',
    links: [
      {
        name: 'user',
        icon: <FaRegUserCircle />,
      },
      {
        name: 'lokasi-terdekat',
        icon: <FaLocationArrow />,
      },
    ],
  },
];

export const menuDirektur = [
  {
    title: 'Dashboard',
    links: [
      {
        name: 'beranda',
        icon: <IoLogoMicrosoft />,
      },
    ],
  },
  {
    title: 'Master',
    links: [
      {
        name: 'property',
        icon: <RiBuilding4Fill />,
      },
      {
        name: 'booking',
        icon: <TbBrandBooking />,
      },
    ],
  },
  {
    title: 'transaksi',
    links: [
      {
        name: 'transaksi',
        icon: <TbReportMoney />,
      },
    ],
  },
  {
    title: 'laporan',
    links: [
      {
        name: 'laporan-transaksi',
        icon: <TbReportAnalytics />,
      },
    ],
  },
];

export const menuSales = [
  {
    title: 'Dashboard',
    links: [
      {
        name: 'beranda',
        icon: <IoLogoMicrosoft />,
      },
    ],
  },
  {
    title: 'Master',
    links: [
      {
        name: 'booking',
        icon: <TbBrandBooking />,
      },
    ],
  },
  {
    title: 'transaksi',
    links: [
      {
        name: 'transaksi',
        icon: <TbReportMoney />,
      },
    ],
  },
];
