export interface propertyDto {
  kode_property: string;
  nama_property: string;
  type_property: string;
  latitude: number;
  longitude: number;
  harga_property: number;
  harga_sewa_property: number;
  stok: number;
  alamat: string;
  picture: string;
  createdAt: string;
  updatedAt: string;
}

export interface facilityPropertyDto {
  kode_property_facility: string;
  kode_facility: string;
  nama_fasilitas?: string;
  kode_property: string;
  value: string;
}

export interface spesificationPropertyDto {
  kode_property_spesification: string;
  kode_spesification: string;
  nama_spesifikasi?: string;
  kode_property: string;
  value: string;
}

export interface locationDto {
  kode_lokasi: string;
  nama_lokasi: string;
  alamat: string;
  latitude: number;
  longitude: number;
  icon: string;
}
