export function _getDistanceFromLatLonInKm(
  lat1: number,
  lng1: number,
  lat2: number,
  lng2: number
) {
  const R = 6371;
  const dLat = deg2rad(lat2 - lat1);
  const dLon = deg2rad(lng2 - lng1);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d;
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180);
}

export function getNextMonthDate() {
  const currentDate = new Date();
  const nextMonthDate = new Date(currentDate);
  nextMonthDate.setMonth(currentDate.getMonth() + 1);
  return nextMonthDate;
}

export function getThreeMonthsLaterDate() {
  const currentDate = new Date();
  const threeMonthsLaterDate = new Date(currentDate);
  threeMonthsLaterDate.setMonth(currentDate.getMonth() + 3);
  return threeMonthsLaterDate;
}

export function getSixMonthsLaterDate() {
  const currentDate = new Date();
  const threeMonthsLaterDate = new Date(currentDate);
  threeMonthsLaterDate.setMonth(currentDate.getMonth() + 6);
  return threeMonthsLaterDate;
}

export function getOneYearLaterDate() {
  const currentDate = new Date();
  const oneYearLaterDate = new Date(currentDate);
  oneYearLaterDate.setFullYear(currentDate.getFullYear() + 1);
  return oneYearLaterDate;
}
