import Home from './pages/home';
import Login from './pages/auth';
import ViewDetailProperty from './pages/property/ViewDetailProperty';
import MainRouter from './utils/DashboardRouter';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { AuthProvider, RequireAuth } from 'react-auth-kit';

function App() {
  return (
    <AuthProvider
      authType={'cookie'}
      authName={'_auth'}
      cookieDomain={window.location.hostname}
      cookieSecure={false}
    >
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            path="/detail-property/:kode_property"
            element={<ViewDetailProperty />}
          />
          <Route path="/auth/login" element={<Login />} />
          <Route
            path="/app/*"
            element={
              <RequireAuth loginPath="/">
                <MainRouter />
              </RequireAuth>
            }
          />
        </Routes>
      </Router>
    </AuthProvider>
  );
}

export default App;
